import {SHAZAM_REQUEST,SHAZAM_SUCCESS,SHAZAM_ERROR} from './types';
import {Actions} from 'react-native-router-flux';
import {ToastAndroid} from 'react-native';
import Expo from 'expo';



/**
 * Executes when user initiates a shazam request
 */
export const ShazamRequest = ()=>({type: SHAZAM_REQUEST});


/**
 * @param {JSON} json
 * initiates when user shazam successful
 */

export const ShazamSuccess = json=> ({
     type: SHAZAM_SUCCESS,
     payload:json,
});


/**
 * Initiates when user shazam failed
 * @param {String} error 
 */
export const ShazamError = (error) => ({
  type: SHAZAM_ERROR,
  payload: error,
});




/**
 * 
 * @param {JSON} shazamData 
 * @function CarShazam
 * Initates car shazam
 */


export const CarShazam = shazamData => dispatch =>{
   

    dispatch(ShazamRequest());
    fetch('',{
      method:'POST',
      headers:{
        'content-type': 'application/json',

      },
      body: JSON.stringify(userData)
    })
    . then(res => res.json())
    . then(data => {

        
      }).catch((error) => {
        ToastAndroid.showWithGravity(
          error.toString(),
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
        );
              Actions.ShazamScreen();
      });
  } 


