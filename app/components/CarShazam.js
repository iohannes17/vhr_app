import React,{Component} from 'react';
import * as Animatable from 'react-native-animatable'
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import {
  StyleSheet,
  Text,
  View,
  Alert,
  ActivityIndicator,
  PermissionsAndroid,
  Platform,
  Image,
  ImageBackground,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import {Camera,Permissions} from 'expo'
import {imageurl} from '../imageUrl'


 
export default class ShazamScreen extends Component {
  
  constructor(props) {
    super(props);
  }

  
  state = {
    switchValue: false ,
    hasCameraPermission: null, //Permission value
    type: Camera.Constants.Type.back, //specifying app start with back camera.
    showCamera:false,
  };
  
  async componentWillMount() {
    //Getting Permission result from app details.
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }


 onPress = () =>{
   this.setState({
     showCamera:true
   })
 }


 snap = async () => {
  if (this.camera) {
    let photo = await this.camera.takePictureAsync();
    console.log(photo);
    this.setState({
      showCamera:false
    })
  }
};















  render() {

 
    
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    }
    
    else if (hasCameraPermission === false) {
      return (
        <View>
          <Text style={styles.hasNoCameraPermission}>No access to camera</Text>
        </View>
      );
    }

 else {
      return (
        <View style={styles.container}>
        <ImageBackground style={styles.picture} source={{uri:imageurl+'wall.png'}}>
         
        <Text style ={styles.textshazam}>Tap to Carzam</Text> 
         
        {!this.state.showCamera  ?


          <Animatable.View animation="pulse" easing="ease-out" iterationCount="infinite" style={styles.buttonView}>
           <TouchableHighlight style={styles.CircleShapeView} onPress={this.onPress}>
           <Image source={{uri:imageurl+'shazam_icon.png'}} style={{width:wp('35%'),height:hp('16.6%')}}/> 
           </TouchableHighlight>
           </Animatable.View>  
                      
              :
              <Camera style={{ flex: 1,}} type={this.state.type} ref={ref => { this.camera = ref; }}>
              
              <View
              style={{
                flex: 1,
                backgroundColor: 'transparent',
                flexDirection: 'row',
                height:hp('50%'),
               
              }}>
              <TouchableOpacity
                style={styles.snapButton}
                onPress={this.snap}>
                <Text
                  style={styles.cameraSnapText}>
                  Capture
                </Text>
              </TouchableOpacity>
            </View>
              </Camera>
              
        }
                    
      </ImageBackground>
         </View>   
      );
     }
  }
}








const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
   
  },


  buttonView:{
    width: 250,
    height: 250,
    marginTop:hp('10%'),
    borderRadius: 250/2,
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center'
  },
 
  CircleShapeView: {
    width: 200,
    height: 200,
    borderRadius: 200/2,
    backgroundColor: '#339FE7',
    elevation: 2,
    justifyContent:'center',
    alignItems:'center'
  

},

snapButton: {

  flex: 1,
  alignSelf: 'flex-end',
  alignItems: 'center',
  justifyContent:'center',
  bottom:20, 
  left:2,
  width:70,
  height:50,
  bottom:12
  



},
 
OvalShapeView: {
  marginTop: 20,
  width: 100,
  height: 100,
  backgroundColor: '#00BCD4',
  borderRadius: 50,
  transform: [
    {scaleX: 2}
  ]
},

textshazam:{
  fontFamily:'Lato-Bold',
  fontSize:18,
  marginTop:hp('9%'),
  marginBottom:hp('4%'),
  color:'#ffffff',
  textAlign:'center'

},

picture:{
  height:hp('100%'),
    width:wp('100%'),
    
},

hasNoCameraPermission:{
  fontFamily:'Lato-Bold',
  fontSize:30,
  color:'#ffffff',
  textAlign:'center'
},

cameraSnapText:{
  fontFamily:'Lato-Bold',
  fontSize:20,
  color:'#ffffff',
  textAlign:'center',
  elevation:2,
  textAlign:'center'
}
});
