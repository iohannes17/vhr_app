import React, {Component} from 'react';
import Dimensions from 'Dimensions';
import {View,Text, StyleSheet,} from 'react-native';
import * as Animatable from 'react-native-animatable'
import  Wallpaper from './Wallpaper';
import {WebView} from 'react-native'




 export default class Faq extends Component {


  constructor(props){
    super(props);
    this.state ={
        transition:'lightSpeedIn'
    };
    
  }
  


  
  render() {
     
     
    
    return (

       <Wallpaper>

        <Animatable.View style={styles.container} animation={this.state.transition}>
           <WebView
              source={{uri:"https://carfax.ng/#/faqs"}}
           />
          
          </Animatable.View>
      </Wallpaper>
    );
  }
}


const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: DEVICE_WIDTH,
   
  },
});




