/**
 * @export GetForeignData
 * Retrieves the Foreign report
 */


import {FOREIGN_REPORT_SUCCESS,FOREIGN_REPORT_ERROR} from './types';
import Expo from 'expo'
import {Alert} from 'react-native';
import {Actions} from 'react-native-router-flux';




/**
 * 
 * @param {JSON} json 
 */
export const ForeignReportSuccess =json => ({
     type:FOREIGN_REPORT_SUCCESS,
     payload:json,
});


/**
 * @param {String} error
 */

export const ForeignReportError = error => ({
  type:FOREIGN_REPORT_ERROR,
  payload: error,
});



/**
 * @function GetForeignData
 * @param {String} vin
 * @returns {HTMLAllCollection} response
 * Retrieves foreign data from the API
 */

  
export const GetForeignData = vin => {
    return async dispatch =>{

      try{
         
             const value = await Expo.SecureStore.getItemAsync('user');
            let response = await fetch('https://api.carfacts.ng/api/vin/'+ vin,{
                method:'GET',
                headers:{ 
                'content-type': 'application/html',
                'Authorization': 'Bearer '+ value
                },
              
            });
            
       let json = await response.text();

          if(response.status === 200){

            dispatch(ForeignReportSuccess(json));
            Actions.ForeignReportScreen();

          }else if(response === 400){
              
            Alert.alert( '','Could not get data')
            Actions.DashBoardScreen();

          }else{
              
            Alert.alert( '','Could not get Data')
            Actions.DashBoardScreen();
          }
       

      }catch(error){
        dispatch(ForeignReportError(error));
      }
    
    };
   } ;