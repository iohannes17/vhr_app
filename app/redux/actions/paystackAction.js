/**
 * @export Reset
 * @export chargeCard
 * @export saveItem
 * 
 * Consuming PAYSTACK payment API
 */


import {PAYSTACK_SUCCESS,PAYSTACK_ERROR,PAYMENT_REQUEST,RESET} from './types';
import RNPaystack from 'react-native-paystack';
import Expo from 'expo';
import {ToastAndroid} from 'react-native';



/**
 * Initiates payment request
 */

export const PayRequest = ()=>(
  {
    type: PAYMENT_REQUEST,
    payload:''
  }
);



/**
 * @param {JSON} json
 * Executes On User Successful Payment
 * 
 */

export const PaySuccess = json => ({
    type:PAYSTACK_SUCCESS,
    payload:json,
});


/**
 * @param {String} error
 * Executes on Unsuccessful payment
 */

export const PayError = error => ({
 type: PAYSTACK_ERROR,
 payload: error,
});


/**
 * Initializes the reducer state to initial state
 */

export const Reset = () => ({
    type: RESET,
    payload: '',
   });



   /**
    * @function chargeCard
    * @param {JSON} charges
    * @return {JSON} payment reference
    * 
    */

  export const chargeCard = charges =>{

  
    
    return async dispatch =>{
      try{

        let ref;
         
        dispatch(PayRequest());      
     RNPaystack.chargeCard(charges)
     .then(response => {
     
      if(response){
       dispatch(PaySuccess(response.reference));
          saveItem(response.reference)
         
        }})

     .catch(error =>{
        dispatch(PayError(error.message));
        ToastAndroid.showWithGravity(
          error.message,
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
        );
      
     });
 
      }catch(error){
         dispatch(PayError(error));
         ToastAndroid.showWithGravity(
          'Network Error',
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
        );
      }};
   };


   /**
    * @param {string} item
    * Saves the transaction reference  to app memory
    */

    const saveItem = async(item) =>{
    
    await Expo.SecureStore.setItemAsync('app',item)

   }

   

  
 

 
 
 