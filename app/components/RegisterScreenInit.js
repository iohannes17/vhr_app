import React, { Component } from 'react';
import { View, TextInput,Text, ScrollView,Image,Button, Animated,TouchableOpacity, Keyboard, KeyboardAvoidingView,Platform,Alert} from 'react-native';
import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL } from './styles';
import Wallpaper from './Wallpaper';
import {Actions} from 'react-native-router-flux';
import Expo from 'expo';
import {imageurl} from '../imageUrl'

class RegisterScreenTwo extends Component {
  constructor(props) {
    super(props);
    this.state = {
        user: '',
        email   : '',
        first_name: '',
        last_name: '',
        phone_number:'',
      }

    this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
    this._onClickNext = this._onClickNext.bind(this);
  }
  



async _onClickNext (){

    const username = this.state.user
    const email = this.state.email
    const fname = this.state.first_name
    const lname = this.state.last_name
    const phone = this.state.phone_number

    if(username.length < 0 || email.length < 0  ||
       fname.length <0 ||  lname.length <0){

        Alert.alert(
            '',
            'Please fill fields'
          );

       }
       else if(phone.length < 11 ){

        Alert.alert(
            '',
            'Incorrect Phone Number'
          );
       }
       else{

        await Expo.SecureStore.setItemAsync('username',username.toString());
        await Expo.SecureStore.setItemAsync('fname',fname.toString());
        await Expo.SecureStore.setItemAsync('lname',lname.toString());
        await Expo.SecureStore.setItemAsync('email',email.toString());
        await Expo.SecureStore.setItemAsync('phone',phone.toString());
        Actions.RegisterScreen();

       }   
  }




 async componentWillMount () {
   if (Platform.OS=='ios'){
    this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
   }else{
    this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
    this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
   }
    
   await Expo.SecureStore.deleteItemAsync('username');
   await Expo.SecureStore.deleteItemAsync('fname');
   await Expo.SecureStore.deleteItemAsync('lname');
   await Expo.SecureStore.deleteItemAsync('email');
   await Expo.SecureStore.deleteItemAsync('phone');
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  keyboardWillShow = (event) => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: IMAGE_HEIGHT_SMALL,
    }).start();
  };

  keyboardWillHide = (event) => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: IMAGE_HEIGHT,
    }).start();
  };


  keyboardDidShow = (event) => {
    Animated.timing(this.imageHeight, {
      toValue: IMAGE_HEIGHT_SMALL,
    }).start();
  };

  keyboardDidHide = (event) => {
    Animated.timing(this.imageHeight, {
      toValue: IMAGE_HEIGHT,
    }).start();
  };

  render() {
    return (
        <Wallpaper>
      <View style={{flex:1,alignItems:'center'}}>
       
       <Text style={styles.textstyle}>Registration</Text>
       <ScrollView style={{flex:1}} scrollEnabled={true} showsHorizontalScrollIndicator={true}>
      
         <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
      >

       <View style={styles.inputContainer}>
       <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/male-user/ultraviolet/50/3498db'}}/>
      <TextInput
            placeholder="Username"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            value ={this.state.user}
            onChangeText={(user) => this.setState({user})}
            style={styles.input}
          />
         </View>

         <View style={styles.inputContainer}>
       <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/male-user/ultraviolet/50/3498db'}}/>
      <TextInput
            placeholder="First Name"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            value ={this.state.first_name}
            onChangeText={(first_name) => this.setState({first_name})}
            style={styles.input}
          />
         </View>

          <View style={styles.inputContainer}>
       <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/male-user/ultraviolet/50/3498db'}}/>
      <TextInput
            placeholder="Last Name"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            value ={this.state.last_name}
            onChangeText={(last_name) => this.setState({last_name})}
            style={styles.input}
          />
         </View>

         <View style={styles.inputContainer}>
         <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/>
      <TextInput
            placeholder="Email"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            value ={this.state.email}
            onChangeText={(email) => this.setState({email})}
            style={styles.input}
          />
         </View>

             <View style={styles.inputContainer}>
             <Image style={styles.inputIcon}  source={{uri: 'https://png.icons8.com/phone/ultraviolet/50/3498db'}}/>
      <TextInput
            placeholder="Phone Number"
            keyboardType="phone-pad"
            underlineColorAndroid='transparent'
            value ={this.state.phone_number}
            onChangeText={(phone_number) => this.setState({phone_number})}
            style={styles.input}
          />
         </View>

          
      </KeyboardAvoidingView>
      </ScrollView>
      <View>
      <TouchableOpacity style={styles.register} onPress={this._onClickNext}>
               <Text>Next</Text>
      </TouchableOpacity>
      </View>
      </View>
      </Wallpaper>
    );
  }
};

export default RegisterScreenTwo