
import React, { Component } from "react";
import { StyleSheet, FlatList, Text, Image, View,Touch } from "react-native";
import PropTypes from "prop-types";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import * as Animatable from 'react-native-animatable'
import {imageurl} from '../imageUrl'

export default class NotificationCustomListview extends Component {
  _keyExtractor = item => item.created_at;

  _renderItem = ({ item }) => {
    const {title,body} = item;
    return (
      <View>
        <Animatable.View style={styles.cardContainerStyle} animation="lightSpeedIn">
          <View style={{ paddingRight: 3 }}>
            <Text style={styles.messagecardTextStyle}>
              {title} {"\n"}
              {body} {"\n"}
             
            </Text>
          </View>
          <View
            style={styles.faceImageStyle}>
            <Image style={styles.faceImageStyle} source={{uri:imageurl+'dna1.png'}}/>
            </View>
          </Animatable.View>
      </View>
    );
  };


  render() {
    return (
      <FlatList
        style={{ flex: 1, }}
        data={this.props.messageList}
        keyExtractor={this._keyExtractor}
        renderItem={this._renderItem}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
      />
    );
  }
}

NotificationCustomListview.propTypes = {
messageList: PropTypes.array,
};




const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent"
  },
  cardContainerStyle: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    margin: 18,
    backgroundColor: "white",
    padding: 5,
    elevation:3
  },
  faceImageStyle: {
    width: wp('20%'),
    height: hp('10%'),

  },
  messagecardTextStyle: {
    color:'#339FE7',
    textAlign: "left",
    fontStyle:'normal',
    fontWeight:"200",
    fontFamily:'Lato-Bold',
    padding: 5

  }
});

