import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Dimensions from 'Dimensions';
import {StyleSheet, ImageBackground} from 'react-native';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import {imageurl} from '../imageUrl'



export default class Wallpaper extends Component {
  render() {
    return (
      <ImageBackground style={styles.picture} source={{uri:imageurl+'wall.png'}}>
        {this.props.children}
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  picture: {
    flex: 1,
     width:null,
      height:null,
    //  justifyContent:'center',
    //  alignItems:'center'
    
  },
});
