import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Dimensions from 'Dimensions';
import {View,Text, StyleSheet,TouchableOpacity,TouchableHighlight} from 'react-native';
import MaterialIntials from 'react-native-material-initials/native';
import {LinearGradient} from 'expo';
import {Actions,ActionConst} from 'react-native-router-flux';
import {FetchUserInfo } from '../redux/actions/userInfoActions';
import {connect} from 'react-redux';
import Expo from 'expo';
import * as Animatable from 'react-native-animatable'
import  Wallpaper from './Wallpaper';
import { Container, Header, Content, Form, Item, Input, Label } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'




 class ProfileScreen extends Component {


  constructor(props){
    super(props);
    this.state ={
        fname:'',
        lname:'',
        package_active:'',
        transition:'lightSpeedIn'
    };
    
  }
  


  
  render() {
     
     const fname =  this.props.userDetails.userInfoList.first_name;
     const lname =  this.props.userDetails.userInfoList.last_name;
     
    
    return (

       <Wallpaper>

        <Animatable.View style={styles.container} animation={this.state.transition}>
         
           <LinearGradient 
             colors={['rgba(0,0,0,0)','rgba(0,0,0,0)']}
           start={{x:0.0,y:1.0}}
           end={{x:1.0,y:1.0}}
           style={styles.tiles}>
        
           <MaterialIntials
            style={{alignSelf:'center',marginTop:40}}
            backgroundColor={'white'}
            color={'#3d4165'}
            size={80}
            text={fname +" "+ lname}
            single={false}
           /> 
          </LinearGradient>    

            
            <Container style={{backgroundColor:'rgba(0,0,0,0)'}}>
        <Content contentContainerStyle={{justifyContent:'center'}}>
          <Form>
            <Item floatingLabel>
              <Label style={{color:'#fff'}}>First Name</Label>
              <Input />
            </Item>
            <Item floatingLabel last>
              <Label style={{color:'#fff'}}>Last Name</Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label style={{color:'#fff'}}>Address</Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label style={{color:'#fff'}}>Phone</Label>
              <Input />
            </Item>
          </Form>
          
          <Container style={{justifyContent:'center',height:hp('10%'),marginTop:hp('5%'),backgroundColor:'rgba(0,0,0,0)',alignItems:'center'}}>
              <Content>
          <TouchableHighlight 
              style={{alignSelf:'center',width:wp('50%'),backgroundColor:'#fff',justifyContent:'center',height:hp('10%')}
              }>
              <Text style={{color:'#339FE7',fontFamily:'Lato-Bold',textAlign:'center'}}>Update</Text>
          </TouchableHighlight>
          </Content>
          </Container>


        </Content>
      </Container>
        
          </Animatable.View>
      </Wallpaper>
    );
  }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: DEVICE_WIDTH,
    flexDirection:'column',  
  },

  tiles:{
    height:'25%',
    width:'100%',
    alignItems:'center',
    justifyContent:'center',
    opacity:0.8
  },
 


});



const mapStateToProps = state =>{
  return{
      userDetails: state.userinfo,
     
      
      
  };
}

ProfileScreen.propTypes ={
    FetchUserInfo: PropTypes.func.isRequired,
    userDetails: PropTypes.any,
  
}


export default connect(mapStateToProps, {FetchUserInfo})(ProfileScreen);