import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Dimensions from 'Dimensions';
import {StyleSheet, View, TextInput, Image,TouchableOpacity} from 'react-native';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

export default class UserInput extends Component {
  render() {
    return (
      <View style={styles.inputWrapper}>
        <Image source={this.props.source} style={styles.inlineImg} />
        <TextInput
          style={styles.input}
          placeholder={this.props.placeholder}
          secureTextEntry={this.props.secureTextEntry}
          autoCorrect={this.props.autoCorrect}
          autoCapitalize={this.props.autoCapitalize}
          returnKeyType={this.props.returnKeyType}
          placeholderTextColor="white"
          underlineColorAndroid="transparent"
          value={this.props.value}
          onChangeText={this.props.onChangeText}
          
        />
         <TouchableOpacity
          activeOpacity={0.7}
          style={styles.btnEye}
          onPress={this.props.onPress}>

          <Image source={this.props.sources} style={styles.iconEye} />
        </TouchableOpacity>
      </View>
    );
  }
}

UserInput.propTypes = {
  source: PropTypes.any.isRequired,
  placeholder: PropTypes.string.isRequired,
  secureTextEntry: PropTypes.bool,
  autoCorrect: PropTypes.bool,
  autoCapitalize: PropTypes.string,
  returnKeyType: PropTypes.string,
  value:PropTypes.string,
  onChangeText:PropTypes.func,
  onPress:PropTypes.func,
  sources: PropTypes.any,
};

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  input: {
    
    width: wp('80%'),
    height: 50,
    marginHorizontal: 20,
    paddingLeft: 10,
    borderRadius: 0,
    color: 'white',
    marginLeft:wp('15%')
    
  },
  inputWrapper: {
    width: wp('90%'),
    height: 50,
    marginBottom:20,
    justifyContent:'center',
    alignSelf:'center',
    alignItems:'center',
    borderColor:'#fff',
    borderWidth:1,
    backgroundColor:'rgba(0,0,0,0.1)'
  
  },
  inlineImg: {
    position: 'absolute',
    zIndex: 99,
    width: 22,
    height: 22,
    left: 10,
    zIndex: 99,
    top: 9,
    tintColor: '#fff',
  },
  btnEye: {
    position: 'absolute',
    top: 10,
    right: 25,
  },
  iconEye: {
    width: 25,
    height: 25,
    tintColor:'#fff',
  },
});
