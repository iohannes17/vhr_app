/**
 * @export fetchVinTeaser
 * @export fetchVinTeaserError
 * @export fetchVinTeaserSuccess
 * @export AnalyzerError
 * @export AnalyzerSuccess
 * @export RiskAnalyzer
 * @export RESETVIN
 * 
 */


import {FETCH_VIN_TEASER_SUCCESS,FETCH_VIN_TEASER_ERROR,ANALYZER_ERROR,ANALYZER_SUCCESS,RESET,TEASER_REQUEST} from './types';
import {Toast} from 'native-base'



/**
 * @function fetchVinTeaserSuccess
 * @param {string} json
 * @return JSON data
 */

export const fetchVinTeaserSuccess =json => ({
     type:FETCH_VIN_TEASER_SUCCESS,
     payload:json,
});


/**
 * @function fetchVinTeaserError
 * @param {string} error
 * @return string data
 */

export const fetchVinTeaserError = error => ({
  type: FETCH_VIN_TEASER_ERROR,
  payload: error,
});


/**
 * @function AnalyzerSuccess
 * @param {string} json
 * @return string data {YES,NO,NOT AVAILABLE}
 */

export const AnalyzerSuccess =json => ({
  type:ANALYZER_SUCCESS,
  payload:json,
});


/**
 * @function AnalyzerError
 * @param {string} error
 * @return string
 */

export const AnalyzerError = error => ({
type: ANALYZER_ERROR,
payload: error,
});


/**
 * @function ResetVin
 * Resets the state of the reducer to initial state
 */

export const ResetVin = () =>{
  return async dispatch =>{
     dispatch({
       type:RESET
     })
  }
}

export const resetTeaser = () =>{
  return async dispatch =>{
     dispatch({
       type:'RESET_TEASER',
       payload:''
     })
  }
}


export const RequestTeaser = ()=>({type: TEASER_REQUEST});
 

  /**
   * @function fetchVinTeaser
   * @param {string} vin
   * @return {JSON} response
   * Retrieves VIN teaser details from API 
   */

    export const fetchVinTeaser = (vin) =>{
          

      return async dispatch =>{
        try{

          dispatch(RequestTeaser());
         let response = await fetch("https://api.carfacts.ng/api/vincheck/" + vin
    
        );
         let json = await response.text();    
         const obj = JSON.parse(json);

           if(obj.VIN && obj.Make){
             
            dispatch(fetchVinTeaserSuccess(obj));
           
           }else{
          
            Toast.show({
              text:'Invalid Vin/Chassis Number',
              duration:1000
          });
           }

        
        }catch(error){
           dispatch(fetchVinTeaserError(error));
          
           Toast.show({
            text:'Invalid Vin/Chassis Number',
            duration:1000
        });
        }
      
      };
      
     } ;



     /**
   * @function RiskAnalyzer
   * @param {string} vin
   * @return {String} (YES,NO,NOT AVAILABLE)
   * Retrieves VIN analysis from API 
   */

     export const RiskAnalyzer = (vin) =>{
    
      return async dispatch =>{
        try{
   
         let response = await fetch("https://api.carfacts.ng/api/vincheck/" + vin
    
        );
         let json = await response.text();
         const obj = JSON.parse(json);

         dispatch(AnalyzerSuccess(true));

        }catch(error){
           dispatch(AnalyzerError(false));
           ToastAndroid.showWithGravity(
            'Network Error',
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
          );
        }
      
      };
     } ;

