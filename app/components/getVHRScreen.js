import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  ActivityIndicator,
  Dimensions,
  ScrollView
} from 'react-native';
import Wallpaper from './Wallpaper';
import {fetchVinTeaser,ResetVin,resetTeaser,RequestTeaser} from '../redux/actions/vinTeaserActions';
import {connect} from 'react-redux'
import PropTypes from 'prop-types';
import {Card} from 'react-native-elements';
import Expo from 'expo';
import {Actions} from 'react-native-router-flux';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import {imageurl} from '../imageUrl';
import {Toast} from 'native-base'


 class GetVhrScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      vin: '',
    

    }
    this._onPress = this._onPress.bind(this);
    this._getVHR = this._getVHR.bind(this);
   
  }
  

  _onPress(){
     
    const vin = this.state.vin;
    if(vin.length > 17  || vin.length < 15){
      Toast.show({
        text:'Incorrect Vin/Chassis Number',
        duration:1000
    });
    }
    else{
      this.props.RequestTeaser();
      this.props.fetchVinTeaser(vin);
    
    }
   
  }


 async _getVHR(){
   const vinNumber = this.state.vin;
  await Expo.SecureStore.setItemAsync('vin_number',vinNumber);
  Actions.PurchaseVHRScreen();

 }

async componentWillUnmount(){
  this.props.resetTeaser()
  
}


  render() {
     
    let content;
  

      if (!this.props.teaser.isAvailable) {
       
        content = <View></View>;
                                           
      }
      else{
            
       
        content =  <View>
         
                  
           
            <Text style={styles.textteaser}>Teaser Details</Text>

                <Card containerStyle={styles.cardstyle}>
                   <Text style={styles.attStyle}>VIN : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <Text style={styles.propStyle}>{this.props.teaser.TeaserList.VIN}
                 </Text></Text>              
            </Card>

             <Card containerStyle={styles.cardstyle}>
                   <Text style={styles.attStyle}>Make : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <Text style={styles.propStyle}>{this.props.teaser.TeaserList.Make}
                 </Text></Text>              
            </Card>

              <Card containerStyle={styles.cardstyle}>
                   <Text style={styles.attStyle}>Model : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <Text style={styles.propStyle}>{this.props.teaser.TeaserList.Model}
                 </Text></Text>              
            </Card>

            <Card containerStyle={styles.cardstyle}>
                   <Text style={styles.attStyle}>Body Style : &nbsp;&nbsp;
                     <Text style={{color:'#000000',fontSize:10}}>{this.props.teaser.TeaserList.BodyStyle}
                 </Text></Text>              
            </Card>


            <Card containerStyle={styles.cardstyle}>
                   <Text style={styles.attStyle}>Engine Style : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <Text style={styles.propStyle}>{this.props.teaser.TeaserList.EngineType}
                 </Text></Text>              
            </Card>
            <Card containerStyle={styles.cardstyle}>
                   <Text style={styles.attStyle}>Plant Country : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <Text style={styles.propStyle}>{this.props.teaser.TeaserList.ManufacturedIn}
                 </Text></Text>              
            </Card>

            <Card containerStyle={styles.cardstyle}>
                   <Text style={styles.attStyle}>Record : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <Text style={styles.propStyle}>{this.props.teaser.TeaserList.Records}
                 </Text></Text>              
            </Card>


             <View styles={styles.btn}>
            <TouchableHighlight style={{alignSelf:'center',
              height:35,   
              marginTop:10,
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom:20,
              width:100,
              borderRadius:25,
              backgroundColor:'gold',
          

          }}
           onPress={this._getVHR}>

              <Text style={styles.signUpText}>GET VHR</Text>
            </TouchableHighlight>
            </View>

           
            </View>;

      }





    return (
     
      <Wallpaper>
      <ScrollView scrollEnabled={true} >
     

         <View style={{width:DEVICE_WIDTH,height:150, justifyContent:'center',alignContent:'center',marginBottom:10,marginTop:15}}>
         <Image source={{uri:imageurl+'purchase_vhr2.png'}} style={{width:170,height:150, alignSelf:'center'}} />  
         </View>

          
      
         
          <View style={styles.tiles2}>
          <TextInput style={styles.inputs}
              placeholder="VIN/Chassis Number"
              underlineColorAndroid='transparent'
              value={this.state.vin}
              returnKeyType={'done'}
              autoCorrect={false}
              maxLength ={17}
              textAlign={'center'}
              onChangeText={(vin) => this.setState({vin})}/>

          {!this.props.teaser.teaserRequest ?  
                     <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={this._onPress}>
                     <Text style={styles.signUpText}> &nbsp;Get Vin Details</Text>
                </TouchableHighlight>
                 :
              <ActivityIndicator size="large" color={'yellow'} />
                }
            </View>   
              
              <View style={styles.tiles4}> 
              {!this.state.showOverlay && content}
                
              </View>
         
      </ScrollView>   
      </Wallpaper>
    

    );
  }
}


const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: DEVICE_WIDTH,
    height:DEVICE_HEIGHT,
    flexDirection:'row',  
   
  },

  tiles:{
    height:'7%',
    width:'100%',
    alignItems:'center',
    justifyContent:'center',
    marginTop:2
  },
  inputContainer: {
      marginTop:40,
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:350,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      borderBottomColor: '#FFFFFF',
      backgroundColor:'white',
      borderRadius:25,
      width:'90%',
      marginBottom:15,
      marginTop:1,
      fontSize:25,
      fontWeight:'500',
      shadowOpacity:0.5,
      elevation:2,
      paddingLeft:5,
      paddingRight:5,
      textAlign:'center',
      letterSpacing:1,
     
  },
 
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },

  signupButton: {
    backgroundColor: "#fec005",
  },
  signUpText: {
    color: 'white',
  },
  tiles2: {
    height:' 2%',
    width:'100%',
    marginBottom:15,
    marginTop:30,
    alignItems:'center',
    justifyContent:'center'

   
  },
 
  text2: {
    color: 'gold',
    backgroundColor: 'transparent',
    fontSize: 20,
    marginTop:2,
    alignSelf:'center',
    marginBottom:25,
    fontFamily:'System',
    fontWeight:'bold'
  },

  textteaser: {
    color: 'gold',
    backgroundColor: 'transparent',
    fontSize: 20,
    fontWeight:"500",
    alignSelf:'center',
    marginBottom:2,
    marginTop:10,
  },

  tiles4: {
    height:hp('55%'),
   width:'100%',
   alignItems:'center',
   flexDirection:'column',
   marginTop:20
   
 },

btn: {
  height:' 3%',
  width:'100%',
  marginBottom:20,
  alignItems:'center',
  justifyContent:'center',
  marginLeft: 60,
},


 Accstyle: {
  height:'13%',
  width:'100%',
 flexDirection:'column',
},


attStyle:{
   fontSize: 12,
   fontWeight:"500",
   color:'#000000',
   marginBottom:15
},

propStyle:{
  fontSize: 14,
  fontWeight:"900",
  color:'#000000',
},

dalert:{
  width: 400,
  height: '30%',
  alignContent:'center',
  alignItems:'center'
   
},

cardstyle:{
  width:300,
  height:40,
  backgroundColor:'white',
  borderRadius:10
}

});



const mapStateToProps = state =>{
  return{
      teaser: state.vinteaser,
      userpackage:state.userinfo,
      
  };
}

GetVhrScreen.propTypes ={
  fetchVinTeaser: PropTypes.func.isRequired,
  ResetVin: PropTypes.func.isRequired,
  RequestTeaser:PropTypes.func.isRequired,
  resetTeaser: PropTypes.func.isRequired,
  teaser:PropTypes.any.isRequired,
  userinfo:PropTypes.any,
}


export default connect(mapStateToProps, {fetchVinTeaser,ResetVin,resetTeaser,RequestTeaser})(GetVhrScreen);