/**
 * Reducer updates and handles the login state of the user
 */

import {LOGIN_REQUEST,LOGIN_SUCCESS,LOGIN_ERROR} from '../actions/types';



const initialState ={
    isLoggedIn: false,
    errorMessage:'',
    Usertoken:'',
    isFetching:false
};


const loginReducer = (state = initialState, action) =>{

    switch(action.type){

        case LOGIN_REQUEST:
             return{ ...state, isFetching:true};

         case LOGIN_ERROR:
              return{ ...state, isLoggedIn:false,isFetching:true,errorMessage:action.payload};

         case LOGIN_SUCCESS:
         
              return { ...state, isLoggedIn:true,isFetching:true,token:action.payload };

 
         default:
            return state;     
    }

};


export default loginReducer;