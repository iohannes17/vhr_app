/**
 * @export RegisterUser
 * @export RetrievePassword
 */

import {REGISTER_REQUEST,REGISTER_SUCCESS,REGISTER_ERROR,FORGOT_PASSWORD_SUCCESS,FOGOT_PASSWORD_ERROR,PASSWORD_REQUEST} from './types';
import {Actions} from 'react-native-router-flux';
import {Toast} from 'native-base';




/**
 * Called when user initiates a registration request
 */
export const RegisterRequest = () =>({
    type: REGISTER_REQUEST,
    payload:'',
});


/**
 * Called when user initiates a password recovery request
 */

export const PaswordRequest = () =>({
  type:PASSWORD_REQUEST,
  payload:'',
});


/**
 * Called when registration is successful
 * @param {String} json
 */

export const RegisterSuccess = json=> ({
     type: REGISTER_SUCCESS,
     payload:json,
});


/**
 * Called when registration fails
 * @param {String} error
 */

export const RegisterError = (error) => ({
  type: REGISTER_ERROR,
  payload: error,
});



/**
 * Called when password recovery is successful
 * @param {String} json
 */

export const ForgotPasswordSuccess = (json) => ({
  type: FORGOT_PASSWORD_SUCCESS,
  payload:json,
});



/**
 * Called when password recovery fails
 * @param {String} error 
 */
export const ForgotPasswordError = (error) => ({
type: FOGOT_PASSWORD_ERROR,
payload: error,
});







/**
 * @function RegisterUser
 * @param {JSON} userData
 * @return {String} response
 * returns a status 201(OK) from successful registration
 */

export const RegisterUser = userData => dispatch =>{

     RegisterRequest();
   
    fetch('https://api.carfacts.ng/api/auth/individual/signup',{
      method:'POST',
      headers:{
        'content-type': 'application/json',

      },
      body: JSON.stringify(userData)
    })
    . then(res => {
            
       const statusCode = res.status;
       if(statusCode === 201){
          
            dispatch(RegisterSuccess(res.json())); 
             Toast.show({
               text:'Registration Successful...Activate account in email',
               duration:1200
             });
            Actions.LoginScreen();
            
      }
      
       else{

          dispatch(RegisterError(res.json())); 
          Toast.show({
            text:'Account previously registered or Credential Error',
            duration:1200
          });
          Actions.RegisterScreenTwo();
      
        
       }
    })
    .catch((error) => {
      Toast.show({
        text:'Network Error',
        duration:1200
      });
         dispatch(RegisterError(res.json())); 
      });
  } 





/**
 * @function RetrievePassword
 * @param {String} email
 * @return {String} response
 * returns a status 200(OK) from successful registration
 */

  export const RetrievePassword = email => dispatch =>{

    dispatch(PaswordRequest());
  
   fetch('https://api.carfacts.ng/api/auth/password/recovery',{
     method:'POST',
     headers:{
       'content-type': 'application/json',

     },
     body: JSON.stringify(email)
   })
   . then(res => {
           
      const statusCode = res.status;
      if(statusCode === 200){
         
           dispatch(ForgotPasswordSuccess(res.json())); 
           ToastAndroid.showWithGravity(
             'Check email for password recovery',
             ToastAndroid.SHORT,
             ToastAndroid.BOTTOM,
           );
           Actions.LoginScreen();
     }
     
      else{

         dispatch(ForgotPasswordError(res.json())); 
         ToastAndroid.showWithGravity(
           'Email Recovery Error',
           ToastAndroid.SHORT,
           ToastAndroid.BOTTOM,
         );
         Actions.ForgotPasswordScreen();
     
       
      }
   })
   .catch((error) => {
     ToastAndroid.showWithGravity(
       'Network Error',
       ToastAndroid.SHORT,
       ToastAndroid.BOTTOM,
     );
             dispatch(RegisterError(res.json())); 
     });
 } 





          

  