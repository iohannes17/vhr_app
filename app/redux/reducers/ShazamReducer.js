/**
 * Reducer updates and handles the login state of the user
 */

import {SHAZAM_ERROR,SHAZAM_REQUEST,SHAZAM_SUCCESS} from '../actions/types';



const initialState ={
    isFetching: false,
    shazamData:[],
    errorMessage:''
   
};


const ShazamReducer = (state = initialState, action) =>{

    switch(action.type){

        case SHAZAM_REQUEST:
             return{ ...state, isFetching:true};

         case SHAZAM_ERROR:
              return{ ...state, isFetching:true,errorMessage:action.payload};

         case SHAZAM_SUCCESS:
         
              return { ...state, isFetching:true,shazamData:action.payload };

        default:
            return state;     
    }

};


export default ShazamReducer;