import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View, Text, Image} from 'react-native';
import {imageurl} from '../imageUrl'


export default class Logo extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image source={{uri:imageurl+'dnaa.png'}} style={styles.image} />
          </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:80,
  },
  image: {
    width: 120,
    height: 100,
    shadowOpacity:1,
  
  },
  text: {
    color: 'white',
    backgroundColor: 'transparent',
    marginTop: 20,
    fontWeight:"400",
    fontFamily:'Lato-Bold',
   fontSize:20
  },
});
