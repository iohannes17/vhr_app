import {LOGIN_REQUEST,LOGIN_SUCCESS,LOGIN_ERROR} from './types';
import {Actions} from 'react-native-router-flux';
import {ToastAndroid} from 'react-native';
import Expo from 'expo';



/**
 * Executes when user initiates a login request
 */
export const UserloginRequest = ()=>({type: LOGIN_REQUEST});


/**
 * @param {JSON} json
 * initiates when user login successful
 */

export const UserloginSuccess = json=> ({
     type: LOGIN_SUCCESS,
     payload:json,
});


/**
 * Initiates when user login failed
 * @param {String} error 
 */
export const UserloginError = (error) => ({
  type: LOGIN_ERROR,
  payload: error,
});




/**
 * 
 * @param {JSON} userData 
 * @function UserLogin
 * Initates user login
 */


export const UserLogin = userData => dispatch =>{
   

    dispatch(UserloginRequest());
    fetch('https://api.carfacts.ng/api/auth/login',{
      method:'POST',
      headers:{
        'content-type': 'application/json',

      },
      body: JSON.stringify(userData)
    })
    . then(res => res.json())
    . then(data => {

        if(data.token){
          dispatch(UserloginSuccess(data.token)); 
          Expo.SecureStore.setItemAsync('user',data.token);
          Expo.SecureStore.setItemAsync('LoginUsername',userData.username);
          Expo.SecureStore.setItemAsync('LoginPassword',userData.password);
          
              Actions.DashBoardScreen();
        }


        else{
           ToastAndroid.showWithGravity(
            'Invalid Login',
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
          )
            
          Actions.LoginScreen();
          dispatch(UserloginError('user invalid credentials'));
        }
      }).catch((error) => {
        ToastAndroid.showWithGravity(
          error.toString(),
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
        );
              Actions.LoginScreen();
      });
  } 


