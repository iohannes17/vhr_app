import {FETCH_VIN_SUCCESS,FETCH_VIN_ERROR} from './types';
import {ToastAndroid} from 'react-native'


export const fetchVinHistorySuccess =json => ({
     type:FETCH_VIN_SUCCESS,
     payload:json,
});

export const fetchVinHistoryError = error => ({
  type: FETCH_VIN_ERROR,
  payload: error,
});


export const fetchVinHistory = id =>{
    
   return async dispatch =>{
     try{

      const value = await Expo.SecureStore.getItemAsync('user');
       
    let response = await fetch('https://api.carfacts.ng/api/vinhistory/' + id,{
      method:'GET',
      headers:{
      'content-type': 'application/xml',
      'Authorization': 'Bearer '+ value
      },
    
  });
        let json = await response.json();
        
       dispatch(fetchVinHistorySuccess(json));
      
     

     }catch(error){
        dispatch(fetchVinHistoryError(error));
        ToastAndroid.showWithGravity(
          'Network Error',
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
        );
     }
   
   };
  } ;



