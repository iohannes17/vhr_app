/**
 * Reducer updates the state of the user status
 */

import {ACTIVE_PLAN_SUCCESS,ACTIVE_PLAN_ERROR,} from '../actions/types';



const initialState ={
   activePlanList:'',
   isAvailable: false,
   isFetching: true,
};


const ActivePlanReducer = (state = initialState, action) =>{
   
 
    switch(action.type){


         case ACTIVE_PLAN_ERROR:
              return{...state, activePlanList:action.payload,};

         case ACTIVE_PLAN_SUCCESS:
         
              return {...state,activePlanList:action.payload,};


         default:
            return state;     
    }

};
 export default ActivePlanReducer;