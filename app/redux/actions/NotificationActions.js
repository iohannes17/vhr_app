import {NOTIFICATION_SUCCESS,NOTIFICATION_ERROR,NOTIFICATION_REQUEST} from './types';
import {ToastAndroid} from 'react-native'


/**
 * 
 * @param {array} json 
 * Initiates notification request
 */
export const NotificationRequest = ()=>({type: NOTIFICATION_REQUEST});

/**
 * @param{string} error
 * throws error in notification request
 */

export const NotificationError = error => ({
  type: NOTIFICATION_ERROR,
  payload: error,
});



/**
 * @param {array} data
 * returns array of objects
 */

export const NotificationSuccess = data => ({
    type: NOTIFICATION_SUCCESS,
    payload: data,
  });
  




export const NotificationHistory = () =>{
    
   return async dispatch =>{
     try{



       dispatch(NotificationRequest());
      const value = await Expo.SecureStore.getItemAsync('user');
       
    let response = await fetch('https://api.carfacts.ng/api/notifications/',{
      method:'GET',
      headers:{
      'content-type': 'application/xml',
      'Authorization': 'Bearer '+ value
      },
    
  });
        let json = await response.json();
        
       dispatch(NotificationSuccess(json.data));
      
     

     }catch(error){
        dispatch(NotificationError(error));
        ToastAndroid.showWithGravity(
          'Network Error',
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
        );
     }
   
   };
  } ;



