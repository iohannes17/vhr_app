import React, { Component } from 'react'
import {Dimensions,View, TextInput,Text,
   ScrollView,Image,Button, Animated,TouchableOpacity, 
   Keyboard, KeyboardAvoidingView,Platform,
   Alert,ActivityIndicator} from 'react-native';
import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL } from './styles';
import Expo from 'expo';
import Spinner from 'react-native-loading-spinner-overlay';
import PropTypes from 'prop-types';
import {chargeCard,Reset} from '../redux/actions/paystackAction';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import * as Animatable from 'react-native-animatable'
import {imageurl} from '../imageUrl'






  class Paystackk extends Component {

    constructor(props){
     
      
        super(props);
        this.state ={
            cardNumber:'',
            email:'',
            expirymonth:'',
            expiryyear:'',
            cvc:'',
            amountInnkobo:'',
            pricetag:0,
            showOverlay:false,
            appPay:'',
            
          
          
        };
        
         this.charge = this.charge.bind(this);
         this._payOk = this._payOk.bind(this);
        this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
       
       
      }




       _payOk(){
          
        this.props.Reset();
        Actions.VerifyPay();
      }



    async storeCard(text){
      this.setState({
        cardNumber: text
      });
      await Expo.SecureStore.setItemAsync('number',text.toString());
    }


    async storemonth(text){
      this.setState({
        expirymonth: text
      });
      await Expo.SecureStore.setItemAsync('month',text.toString());
    }

    async storeyear(text){
      this.setState({
        expiryyear: text
      });
      await Expo.SecureStore.setItemAsync('year',text.toString());
    }
    async storecvc(text){
      this.setState({
        cvc: text
      });
      await Expo.SecureStore.setItemAsync('cvc',text.toString());
    }

    async storeemail(text){
      this.setState({
        email: text
      });
      await Expo.SecureStore.setItemAsync('email',text.toString());
    }

     


  async charge(){


    
    const inprice = this.state.pricetag * 100;
    const number =  this.state.cardNumber;
    const month = this.state.expirymonth;
    const year =  this.state.expiryyear
    const cvc = this.state.cvc
    const email = this.state.email;
 

    if(number.length < 16){

      Alert.alert(
        '',
         'Card Number incomplete',
       
      
      )
    }
   
    else if( cvc.length < 3 ){
      Alert.alert(
        '',
         'CVV incomplete',
       
        
      )

    }

    else if(email.length < 0){
       
      Alert.alert(
        '',
         'Email Address incorrect',  
      )
    }

    else if(year.length < 2){
      Alert.alert(
        '',
         'Please enter valid year',  
      
      )

    }


    else{

     

 

    const charges = {
     
      cardNumber:number,
      expiryMonth:month,
      expiryYear:year,
      cvc:cvc,
      email:email,
      amountInKobo:inprice
     
   };

   this.props.chargeCard(charges);

  }  
  
  }


  componentWillUnmount() {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  keyboardWillShow = (event) => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: IMAGE_HEIGHT_SMALL,
    }).start();
  };

  keyboardWillHide = (event) => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: IMAGE_HEIGHT,
    }).start();
  };


  keyboardDidShow = (event) => {
    Animated.timing(this.imageHeight, {
      toValue: IMAGE_HEIGHT_SMALL,
    }).start();
  };

  keyboardDidHide = (event) => {
    Animated.timing(this.imageHeight, {
      toValue: IMAGE_HEIGHT,
    }).start();
  };

  async componentWillMount () {

  

    if (Platform.OS=='ios'){
     this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
     this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
    }else{
     this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
     this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
    }

     await Expo.SecureStore.deleteItemAsync('number');
    await Expo.SecureStore.deleteItemAsync('month');
    await Expo.SecureStore.deleteItemAsync('year');
    await Expo.SecureStore.deleteItemAsync('cvc');
    await Expo.SecureStore.deleteItemAsync('email');
    
     
   }

   async componentDidMount (){

    const price = await Expo.SecureStore.getItemAsync('price');
    const type = await Expo.SecureStore.getItemAsync('type');
     let newprice = parseInt(price) * parseInt(type);
     const vat = newprice * 0.05; 
     newprice = newprice + vat;
     
    
    this.setState({
      pricetag:newprice,
    });

    
   }
 




   render() {

        const price = this.state.pricetag === '' ? 'Not available' : this.state.pricetag;
              

    return (
        <Animatable.View style={{flex:1,backgroundColor:'#fff',alignContent:'center',alignItems:'center'}} animation="slideInLeft" animation="flipInY">
          <Spinner
          visible={this.props.processresponse}
          textContent={'Initiating Payment...'}
          textStyle={{color:'#fff'}}
          cancelable={true}
           />

           {this.props.alertbox &&
          Alert.alert(
            '',
             this.props.payresponse,
            [
              {text: 'OK', onPress:this._payOk},
            ],
            { cancelable: false }
          )}
    

    
       
      <View style={{flex:1,backgroundColor:'#ffffff',alignItems:'center'}}> 
       <Animated.Image source={require('../../assets/pay.jpg')} style={[styles.logo, { height: this.imageHeight }]} />
       <Text style={styles.pricetext}><Text style={{color:'#b6b6b6',fontWeight:"500",padding:6}}>Pay </Text>NGN {price}</Text>

       <ScrollView style={{flex:1,marginTop:10, elevation:10}} scrollEnabled={true} showsHorizontalScrollIndicator={false}>
       <KeyboardAvoidingView
        style={styles.keyboardcontainer}
        behavior="padding"
      >

       <View style={styles.payinputContainer}>
       <Text style={styles.innerpaytext}>Card Number</Text>
      <TextInput
            placeholder="0000-0000-0000-0000"
            keyboardType="phone-pad"
            underlineColorAndroid='transparent'
            value ={this.state.cardNumber}
            onChangeText={(text) =>this.storeCard(text)} 
            style={styles.payinput}
          />
         </View>

         <View style={styles.yearpayinputContainer}>
        <Text style={styles.innerpaytext}>Expiry Date</Text>
      <TextInput
            placeholder="  MM"
            keyboardType="phone-pad"
            underlineColorAndroid='transparent'
            value ={this.state.expirymonth}
            onChangeText={(month) => this.storemonth(month)}
            style={styles.yearpayinput}
            maxLength={2}
          />

          <TextInput
            placeholder=" YY"
            keyboardType="phone-pad"
            underlineColorAndroid='transparent'
            value ={this.state.expiryyear}
            onChangeText={(year) => this.storeyear(year)}
            style={styles.yearpayinput}
            maxLength={2}
          />
         </View>

          <View style={styles.cvvContainer}>
          <Text style={styles.innerpaytext}>CVV2</Text>
      <TextInput
            placeholder="     000"
            keyboardType="phone-pad"
            underlineColorAndroid='transparent'
            value ={this.state.cvc}
            onChangeText={(cvc) => this.storecvc(cvc)}
            style={styles.payinput}
            maxLength={3}
          />
         </View>

         <View style={styles.payinputContainer}>
         <Image style={styles.inputIcon} source={{uri:imageurl+'envelope.png'}}/>
      <TextInput
            placeholder="    Email"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            value ={this.state.email}
            onChangeText={(email) => this.storeemail(email)}
            style={styles.payinput}
          />
         </View>

         

       <Text style={styles.vat_text}>[VAT INCLUSIVE]</Text>
  </KeyboardAvoidingView>
      </ScrollView>
      <View>
      <TouchableOpacity style={styles.paynow}
       onPress={this.charge} 
       activeOpacity={0.5}>

               <Text style={styles.buttontext}>PURCHASE</Text>
      </TouchableOpacity>
      </View>
      <View>
      <Animated.Image source={{uri:imageurl+'white.png'}} style={styles.paystackdownlogo} />
      </View>
      </View>
      </Animatable.View>
    );
  }
};



const mapStateToProps = state =>{
  return{
      payresponse: state.response.payresponse,
      processresponse:state.response.processing,
      alertbox: state.response.alertme,
      paykey:state.response.apiresult,
     
  };
}

Paystackk.propTypes ={
  chargeCard: PropTypes.func.isRequired,
  Reset: PropTypes.func.isRequired,
  payresponse: PropTypes.any,
  processresponse:PropTypes.bool.isRequired,
  alertbox:PropTypes.bool.isRequired,
  

 
}



export default connect(mapStateToProps, {chargeCard,Reset})(Paystackk);





