import React, {Component} from 'react';
import {View,Text, StyleSheet,ScrollView,ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';
import Expo from 'expo';
import PropTypes from "prop-types";
import * as Animatable from 'react-native-animatable'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import NotificationCustomListview  from './NotificationList';
import {NotificationHistory } from '../redux/actions/NotificationActions'






 class NotificationScreen extends Component {


  constructor(props){
    super(props);
    this.state ={
        fname:'',
        lname:'',
        package_active:'',
        transition:'lightSpeedIn'
    };
    
  }
  

 componentDidMount() {
   this.props.NotificationHistory()
 }
 

  
  render() {
     let content = <NotificationCustomListview messageList={this.props.notificationList} />;

    if (this.props.notificationLoading) {
      content = 
                        <View style={styles.container}>
                             <ActivityIndicator size="large" color="gold"/>
                            </View>;
                          
    }

    return (
     
        <ScrollView contentContainerStyle={styles.container} transition={this.state.transition}>
       
            {content}
      </ScrollView>
       
    );
  }
}

const styles = StyleSheet.create({
container: {
  flex: 1, 
  justifyContent:'center',
  height:hp('100%'),
  width:wp('100%')
},
indicator:{
  flex: 1, 
  justifyContent:'center',
  alignItems:'center',
  alignContent:'center',


},
});



const mapStateToProps = state =>{
  return{
      notificationList: state.NotificationData.data,
      notificationLoading: state.NotificationData.isFetching,
     
  };
}

NotificationScreen.propTypes ={
  NotificationHistory:PropTypes.func.isRequired,
  notificationLoading:PropTypes.bool,
  notificationList:PropTypes.array.isRequired
  
}


export default connect(mapStateToProps, {NotificationHistory})( NotificationScreen);