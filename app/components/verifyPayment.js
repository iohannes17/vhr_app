import React, { Component } from 'react'
import {Dimensions,
    View, 
    Text,
    Alert,
    ActivityIndicator} from 'react-native';

import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL } from './styles';
import PropTypes from 'prop-types';
import {ConfirmPayment,Reset,getForeign,getLocal,fetchVinTeaser,RiskAnalyzer} from '../redux/actions/paymentAction';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';



 class VerifyPay extends Component {



    constructor(props){
 
        super(props);
        this.state ={
         
          isLoading:true      
        };
        
        this._payOk = this._payOk.bind(this);
      }


    componentWillMount(){
     this.props.ConfirmPayment();
    }


    async componentDidMount(){
        const ref = await Expo.SecureStore.getItemAsync('app')

    }


   async _payOk(){  
       
       const vin = await Expo.SecureStore.getItemAsync('vin_number');
       const packageType = await Expo.SecureStore.getItemAsync('packageType');

       if(packageType === 'local'){

        this.props.getLocal(vin);

       }

       else if(packageType === 'riskanalyzer'){

        this.props.fetchVinTeaser(vin);
        this.props.RiskAnalyzer(vin);
       }  

       else{

        this.props.getForeign(vin);
       }
       this.props.Reset();
    }
      
 



  render() {
    return (
     <View style={styles.container}>
       
           <ActivityIndicator  size="large" color="#339FE7" animating={true}/>

             {this.props.isVerified &&
          Alert.alert(
            '',
            this.props.results,
            [
              {text: 'Click to get Report', onPress:this._payOk},
            ],
            { cancelable: false }
          )}

         <Text style={styles.buttontext}>Completing Transaction</Text>

     </View>
    )
  }
}



const mapStateToProps = state =>{
    return{
       isVerified:state.Confirmpayment.isVerified,
       isChecking: state.Confirmpayment.isChecking,
       results: state.Confirmpayment.paymentcheck
       
    };
  }
  
  VerifyPay.propTypes ={
    ConfirmPayment:PropTypes.func.isRequired,
    Reset:PropTypes.func.isRequired,
    fetchVinTeaser:PropTypes.func.isRequired,
    RiskAnalyzer:PropTypes.func.isRequired,
    getForeign:PropTypes.func.isRequired,
    getLocal:PropTypes.func.isRequired,
    isVerified:PropTypes.bool,
    isChecking:PropTypes.bool,
    results:PropTypes.any,
   
  }
  
  
  
  export default connect(mapStateToProps, {ConfirmPayment,Reset,getForeign,getLocal,fetchVinTeaser,RiskAnalyzer})(VerifyPay);
  