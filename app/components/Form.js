import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Dimensions from 'Dimensions';
import {
  StyleSheet,
  KeyboardAvoidingView,
  View,
  Alert,
  AlertIOS,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  Animated,
  Easing,
  Text,
  ToastAndroid
  
} from 'react-native';
import UserInput from './UserInput';
import {Actions} from 'react-native-router-flux';
import {UserLogin} from '../redux/actions/loginActions';
import {connect} from 'react-redux';
import {BarIndicator} from 'react-native-indicators';
import TouchID from "react-native-touch-id";
import Expo from 'expo';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import {imageurl} from '../imageUrl'







const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const MARGIN = 40;

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPass: true,
      press: false,
      isLoggedIn: false,
      isLoading: false,
      username:'',
      password:'',
      biometryType:'',
    };
    this.showPass = this.showPass.bind(this);
    this.buttonAnimated = new Animated.Value(0);
    this.growAnimated = new Animated.Value(0);
    this._onPress = this._onPress.bind(this);
  
  }

  showPass() {
    this.state.press === false
      ? this.setState({showPass: false, press: true})
      : this.setState({showPass: true, press: false});
  }




   _onPress() {
     
   if(this.state.username.length < 1 || this.state.password.length < 8 ){
       
     Alert.alert(
       'User Validation',
       'Incorrect username or password'
     );
   }
   else{
    
    const loginParam ={
      username: this.state.username,
      password: this.state.password
    }



    this.props.UserLogin(loginParam)
    
    if (this.state.isLoading) return;

    this.setState({isLoading:true});

    Animated.timing(this.buttonAnimated, {
      toValue: 1,
      duration: 200,
      easing: Easing.linear,
    }).start();

    setTimeout(() => {
      this._onGrow();
    }, 2000);


    setTimeout(() => {
       
      this.setState({
         isLoading: this.props.isFetching,
        isLoggedIn:this.props.isLoggedIn
      });
      this.buttonAnimated.setValue(0);
      this.growAnimated.setValue(0);


      
    }, 2300);
    

   }
  
  }
  

  _onGrow() {
    Animated.timing(this.growAnimated, {
      toValue: 1,
      duration: 200,
      easing: Easing.linear,
    }).start();
 
  }


  async componentDidMount() {

  const uusername = await Expo.SecureStore.getItemAsync('LoginUsername');
  const upassword = await Expo.SecureStore.getItemAsync('LoginPassword');;

  if(uusername === '' || upassword === ''){

    return;
  }

  else{

    const optionalConfigObject = {
      unifiedErrors:true,
      passcodeFallback:true
    }
     
    TouchID.isSupported()
    .then(biometryType => {
     this.setState({ biometryType });
      this.clickHandler(uusername,upassword);
    }
    
    )
    .catch(error =>{
      ToastAndroid.showWithGravity(
        'TouchID not supported',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
    });

  }
}



clickHandler(user,pass) {
  TouchID.isSupported()
    .then(() => {
      this.setState({
        isLoading:true
      })
      this.authenticate(user,pass)
    })
  
    .catch(error => {
      ToastAndroid.showWithGravity(
        'TouchID not supported',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
      this.setState({
        isLoading:false
      })
    });
}




authenticate = async (user,pass) => {
      
  const loginParam ={
    username: user,
    password: pass
  }

  return TouchID.authenticate()
    .then(success => {
     
      ToastAndroid.showWithGravity(
        'User Authenticated',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
     
      this.setState({
        isLoading:false
      })
      this.props.UserLogin(loginParam)
    })
    .catch(error => {
  
      ToastAndroid.showWithGravity(
        'LOGIN ERROR',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
      this.setState({
        isLoading:false
      })
    });
}





  render() {


    const changeWidth = this.buttonAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [DEVICE_WIDTH - MARGIN, MARGIN],
    });
    const changeScale = this.growAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [1, MARGIN],
    });

    return (
     
      <KeyboardAvoidingView behavior="padding" style={styles.container}>

        <UserInput
         source={{uri:imageurl+'username.png'}}
          placeholder="Username"
          autoCapitalize={'none'}
          returnKeyType={'done'}
          autoCorrect={false}
          value={this.state.username}
          onChangeText={(username)=> this.setState({username})}
          
          
        />

        <UserInput
          source={{uri:imageurl+'password.png'}}
          secureTextEntry={this.state.showPass}
          placeholder="Password"
          returnKeyType={'done'}
          autoCapitalize={'none'}
          autoCorrect={false}
          value={this.state.password}
          onChangeText={(password)=> this.setState({password})}
          onPress={this.showPass}
          sources={{uri:imageurl+'eye_black.png'}}
          
        />
         
         <View style={styles.containerBtn}>
         <Animated.View style={{width: changeWidth}}>
          <TouchableOpacity
            style={styles.button}
            onPress={this._onPress}
            activeOpacity={1}>
            {this.state.isLoading ?  (<BarIndicator color='white' size={10} />) : (<Text style={styles.text}>LOGIN</Text>)}
          </TouchableOpacity>
        </Animated.View>
         </View>
      </KeyboardAvoidingView>
        
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop:hp('10%'),
  },
  btnEye: {
    position: 'absolute',
    top: 80,
    right: 30,
  },
  iconEye: {
    width: 25,
    height: 25,
    tintColor: 'rgba(0,0,0,0.2)',
  },
  containerBtn: {
    alignItems: 'center',
    marginTop:10,
    justifyContent:'center',

  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fec005',
    height: MARGIN,
    width: 150,
    borderRadius: 0,
    alignSelf:'center'
  
  },
  circle: {
    height: MARGIN,
    width: MARGIN,
    marginTop: -MARGIN,
    borderWidth: 1,
    borderColor: '#F035E0',
    borderRadius: 100,
    alignSelf: 'center',
    zIndex: 99,
    backgroundColor: '#F035E0',
  },
  text: {
    color: 'white',
    backgroundColor: 'transparent',
  },
  image: {
    width: 24,
    height: 24,
  },
});


const mapStateToProps = state =>{
  return{
      isLoggedIn: state.userLog.isLoggedIn,
      isFetching: state.userLog.isFetching,
      token: state.userLog.login_token,
  };
}

Form.propTypes ={
  UserLogin: PropTypes.func.isRequired,
  isLoggedIn:PropTypes.bool.isRequired,
  isFetching:PropTypes.bool.isRequired,
  token: PropTypes.string
}



export default connect(mapStateToProps, {UserLogin})(Form);