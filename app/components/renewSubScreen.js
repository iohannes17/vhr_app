
/**
 * @class VHR Purchase
 * Gives the users options to purchase foreign and local data
 */



import React, {Component} from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity,Dimensions,Alert} from 'react-native';
import Wallpaper from './DashBoard';
import {LinearGradient} from 'expo';
import {Actions,ActionConst} from 'react-native-router-flux';
import AwesomeAlert from 'react-native-awesome-alerts';
import Expo from 'expo';
import {Toast} from 'native-base'
import PropTypes from 'prop-types';
import {FetchActivePlan} from '../redux/actions/activePlan';
import {connect} from 'react-redux';
import * as Animatable from 'react-native-animatable'
import {imageurl} from '../imageUrl'






  class RenewSubscriptionScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
    
      showAlert:false,
      price:''
    };
 
    this._showAlert = this._showAlert.bind(this);
    this._hideAlert = this._hideAlert.bind(this);
   this._onPress = this._onPress.bind(this);

  }


/**
 * @this {_onPress}
 * @param {string} price
 * @param {number} type
 * @param {string} packageT (Local,foreign)
 * 
 * Saves these parameters in app memory
 * and transitions to the payment page
 */


_onPress = async (price,type,packageT) => {
  await Expo.SecureStore.setItemAsync('price',price)
  await Expo.SecureStore.setItemAsync('type',type)
  await Expo.SecureStore.setItemAsync('packageType',packageT)
  Actions.Payment();

}

_onPressAlert = () => {
  
  Toast.show({
    text:'Dna4Wheels Report will be available in the next Update',
    buttonText:'',
    duration:1500
});
}



/**
 * @this {_showAlert}
 * changes the state of the  alert dialog to true
 */
  _showAlert (){
    this.setState({showAlert:true});
  };


  /**
 * @this {_hideAlert}
 * changes the state of the  alert dialog to false
 */
  _hideAlert(){
    this.setState({showAlert:false});
  };




  render() {
    

   const showAlert = this.state.showAlert;

    let content;
    content =   
             
               <View>
              <LinearGradient 
                colors={['#ffffff','#ffffff']}
              start={{x:0.0,y:1.0}}
              end={{x:1.0,y:1.0}}
              style={styles.grad}
              >
                <Image source={{uri:imageurl+'local_1_vhr2.png'}} style={styles.inlineImglocalReport} />
                <TouchableOpacity
                style={styles.btn1}
                onPress={()=> this._onPressAlert()}
                activeOpacity={1}>
                  <Text style={styles.textbtn}>  N3000/Report {'\n'}Valid for 30 days</Text>    
              </TouchableOpacity>
              <Image source={{uri:imageurl+'dna.png'}} style={styles.inlineImgdnaIcon} />  
              </LinearGradient>    
                
              <LinearGradient 
                colors={['#ffffff','#ffffff']}
              start={{x:0.0,y:1.0}}
              end={{x:1.0,y:1.0}}
              style={styles.grad}
              >
                <Image source={{uri:imageurl+'foreign_1_vhr2.png'}} style={styles.inlineImgforeignIcon} />
                <TouchableOpacity
                style={styles.btn1}
                onPress={()=> this._onPress('4000','1','foreign')}
                activeOpacity={1}>
                  <Text style={styles.textbtn}>  N4000/Report {'\n'}Valid for 30 days</Text>    
              </TouchableOpacity>
              <Image source={{uri:imageurl+'carfax_logo.png'}} style={styles.inlineImgcarfaxIcon} />  
              </LinearGradient>   


               <LinearGradient 
                colors={['#ffffff','#ffffff']}
              start={{x:0.0,y:1.0}}
              end={{x:1.0,y:1.0}}
              style={styles.grad}
              >
                <Image source={{uri:imageurl+'call.png'}} style={styles.inlineImgphoneIcon} />
                <TouchableOpacity
                style={styles.btn1}
                onPress={()=> this._showAlert()}
                activeOpacity={1}>
                  <Text style={styles.textbtn}>Business Customer{'\n'}(5 VHR and Above)</Text>    
              </TouchableOpacity>
              <Image source={{uri:imageurl+'Coin.png'}} style={styles.inlineImgCoinIcon} />  
              </LinearGradient>   
              </View>;
    
    







    return (
        <Wallpaper>
      <Animatable.View style={styles.container} animation="bounceIn">

            
           <LinearGradient 
             colors={['transparent','transparent']}
           start={{x:0.0,y:1.0}}
           end={{x:1.0,y:1.0}}
           style={styles.tiles}
          >
               <Image source={{uri:imageurl+'renew_sub_2.png'}} style={{height:165,width:165}} />       
          </LinearGradient>    

         
            <View styles={styles.tiles2}>
             <Text style={{color:'#339FE7',fontSize:16,justifyContent:'center',alignSelf:'center', backgroundColor:'#ffffff',}}>Click to Select Plan</Text>
          </View> 

             <View style={styles.tiles4}>{!this.state.showAlert && content}

                <AwesomeAlert
                  show={showAlert}
                  showProgress={false}
                  title="Business Package Subscription"
                  message= "Please Call Customer Support: 07000CARFAX"
                  closeOnTouchOutside={false}
                  closeOnHardwareBackPress={false}
                  showConfirmButton={true}
                  confirmText="Got it"
                  confirmButtonColor="#DD6B55"
                  alertContainerStyle = {styles.dalert}
                  onConfirmPressed ={this._hideAlert}
                  overlayStyle={{width:390,height:500}}
       
                  />
             </View>   
                 
      </Animatable.View>
      </Wallpaper>
    );
  }
}


const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: DEVICE_WIDTH,
    height:DEVICE_HEIGHT,
    flexDirection:'column',  
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor:'#ffffff'
  },

  actionButton:{
   fontSize:20,
   height:22,
   color:'white',
  },
  
  dalert:{
    width: 400,
    height: '30%',
    alignContent:'center',
    alignItems:'center'
     
  },

  image: {
    width: 80,
    height: 80,
  },
  
 
  tiles:{
    height:'22%',
    width:'100%',
    borderBottomRightRadius:39,
    borderBottomLeftRadius:39,
    alignItems:'center',
    justifyContent:'center',
  },
  backgroundImage:{
    flex: 1,
    position:'absolute',
    width:'100%',
    height:'100%',
    alignItems:'center',
    justifyContent: 'center',
    marginTop:20
  },
  tiles2: {
    height:' 5%',
    width:'100%',
    marginBottom:20,
     justifyContent:'center',
     alignContent:'center',
   
  },

  Accstyle: {
    height:'13%',
    width:'100%',
   flexDirection:'column',
  },



  tiles4: {
     height:'60%',
    width:'100%',
    alignItems:'center',
    marginTop:20
    
  },
 
  text: {
    color: 'gold',
    backgroundColor: 'transparent',
    fontWeight: 'bold',
    fontSize: 20,
    marginTop:35,
    alignSelf:'center',
    marginBottom:20,
    marginTop:20,
  },

  text2: {
    color: 'white',
    backgroundColor: 'transparent',
    fontWeight: 'bold',
    fontSize: 20,
    marginTop:35,
    alignSelf:'center',
    marginBottom:20,
    marginTop:20,
  },

  

  textbtn: {
    color: '#339FE7',
    backgroundColor: 'transparent',
    fontWeight: 'bold',
    fontSize: 10,
    alignSelf:'center' ,
    marginTop:7, 
    alignSelf:'center'
  },



  inlineImg: {
    position: 'absolute',
    zIndex: 89,
    width: 32,
    height: 32,
    left: 35,
    top: 35,
  },

  inlineImg2: {
    position: 'absolute',
    zIndex: 89,
    width: 32,
    height: 32,
    right: 35,
    top: 35,
  },

  btn1:{
      alignItems: 'center',
      justifyContent: 'center',
      height: 69,
      width: 300,
      borderRadius: 24,
      zIndex: 100,
      marginBottom: 20, 
  },

  grad:{
    alignItems: 'center',
    justifyContent: 'center',
    height: 85,
    width: 300,
    borderRadius: 24,
    zIndex: 100,
    marginBottom: 20, 
    shadowColor:'#000',
    shadowOffset:{width:0,height:1},
    shadowOpacity:0.9,
    shadowRadius:3,
    elevation:7,

},





buttonLogin:{
  backgroundColor: 'transparent',
  width:'40%',
  borderRadius: 25,
  justifyContent:'center'
},

inlineImgcarfaxIcon: {
  position: 'absolute',
  zIndex: 99,
  width: 50,
  height: 65,
  left: 10,
  top: 15,
},

inlineImgforeignIcon: {
  position: 'absolute',
  zIndex: 99,
  width: 75,
  height: 75,
  right: 10,
  top: 0,
},

inlineImgdnaIcon: {
  position: 'absolute',
  zIndex: 99,
  width: 45,
  height: 45,
  left: 10,
  top: 20,
},

inlineImgCoinIcon: {
  position: 'absolute',
  zIndex: 99,
  width: 45,
  height: 45,
  left: 10,
  top: 20,
},

inlineImglocalReport: {
  position: 'absolute',
  zIndex: 99,
  width: 55,
  height: 74,
  right: 24,
  top: 0,
},

inlineImgphoneIcon: {
  position: 'absolute',
  zIndex: 99,
  width: 65,
  height: 65,
  right: 24,
  top: 10,
},

});


const mapStateToProps = state =>{
  return{
    plans: state.activeplan,
      
  };
}


RenewSubscriptionScreen.propTypes ={ 
    plans:PropTypes.any,    
}


export default connect(mapStateToProps, {FetchActivePlan})(RenewSubscriptionScreen);



