import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Dimensions from 'Dimensions';
import {Button} from 'react-native-elements';
import {StyleSheet, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {heightPercentageToDP as hp,widthPercentageToDP as wp} from 'react-native-responsive-screen'

export default class SignupSection extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Button
         buttonStyle={styles.buttonCreateAccount}
         onPress = {Actions.RegisterScreenTwo}
         titleStyle={styles.text}
        title='Create Account' />


         <Button
         buttonStyle={styles.buttonForgotPassword}
         onPress = {Actions.ForgotPasswordScreen}
         titleStyle={styles.text}
        title='Forgot Password?' />
      </View>
    );
  }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    
    marginBottom: 30,
    width: DEVICE_WIDTH,
    alignSelf:'center',
    justifyContent: 'center',
    flexDirection:'row'
  },
  text: {
    color: 'white',
    backgroundColor: 'transparent',
    fontSize:10
  },

  buttonCreateAccount:{
    justifyContent:'flex-start',
   backgroundColor: 'transparent',
   
    width:wp('30%'),
    marginLeft:wp('20%'),
    marginRight:wp('10%'),
},

buttonForgotPassword:{
  justifyContent:'flex-end',
  backgroundColor: 'transparent',
 
  width:wp('30%'),
  marginLeft:wp('10%'),
  marginRight:wp('20%'),
},
})
