/**
 * Reducer updates the state of PAYSTACK payment gateway
 */

import {PAYSTACK_SUCCESS,PAYSTACK_ERROR,PAYMENT_REQUEST,RESET} from '../actions/types';



const initialState ={
   payresponse:'',
   processing:false,
   alertme:false,
   apiresult:''
};


const PaystackReducer = (state = initialState, action) =>{
   
 
    switch(action.type){


         case PAYSTACK_ERROR:
              return{...state, payresponse:'Payment Failed',processing:false,apiresult:action.payload,};

        case RESET:
              return{...state,processing:false,alertme:false,payresponse:'',};      

         case PAYSTACK_SUCCESS:
         
              return {...state,payresponse:'Continue Processing...', processing:false,alertme:true};

              case PAYMENT_REQUEST:
         
              return {...state,processing:true};
         default:
            return state;     
    }

};
 export default PaystackReducer;