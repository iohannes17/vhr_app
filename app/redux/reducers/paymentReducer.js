/**
 * Reducer updates the state if the payment is successful or unsuccessful
 */

import {PAYMENT_SUCCESS,PAYMENT_ERROR,RESET,REPORT_ERROR,REPORT_SUCCESS} from '../actions/types';



const initialState ={
   paymentcheck:'',
   isVerified:false,
   isChecking:true,
   
};


const PaymentReducer = (state = initialState, action) =>{
   
 
    switch(action.type){


         case PAYMENT_ERROR:
              return{...state, paymentcheck:action.payload,isVerified:true,isChecking:false};

         case PAYMENT_SUCCESS:
         
              return {...state,paymentcheck:action.payload,isVerified:true,isChecking:false};

        case RESET:
             return {...state,isVerified:false,isChecking:false,paymentcheck:''};

        case REPORT_ERROR:
             return{...state,};

        case REPORT_SUCCESS:
        
             return {...state,};
    

         default:
            return state;     
    }

};
 export default PaymentReducer;