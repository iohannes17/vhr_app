/**
 * Reducer updates and handles the login state of the user
 */

import {NOTIFICATION_SUCCESS,NOTIFICATION_ERROR,NOTIFICATION_REQUEST} from '../actions/types';



const initialState ={
    data:[],
   isFetching:false,
   errorMessage:''
};


const NotificationReducer = (state = initialState, action) =>{

    switch(action.type){

        case NOTIFICATION_REQUEST:
             return{ ...state, isFetching:true};

         case NOTIFICATION_ERROR:
              return{ ...state,isFetching:false,errorMessage:action.payload};

         case NOTIFICATION_SUCCESS:
           return { ...state, isFetching:false,data:action.payload };

 
         default:
            return state;     
    }

};


export default NotificationReducer;