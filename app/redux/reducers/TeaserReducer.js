
/**
 * @exports fetchVinTeaserReducer 
 * Teaser Details Reducer
 * Updates states in store
 */


import {FETCH_VIN_TEASER_SUCCESS,FETCH_VIN_TEASER_ERROR,ANALYZER_ERROR,ANALYZER_SUCCESS,RESET,TEASER_REQUEST} from '../actions/types';

const initialState ={
   TeaserList:'',
   isAvailable: false,
   isFetching: true,
   riskAnalyzer: false,
   teaserRequest:false
};


const fetchVinTeaserReducer = (state = initialState, action) =>{
    
 
    switch(action.type){


         case FETCH_VIN_TEASER_ERROR:
              return{...state, TeaserList:action.payload, isAvailable: false,  isFetching:false,teaserRequest:false};

         case FETCH_VIN_TEASER_SUCCESS:
         
              return {...state,TeaserList:action.payload, isAvailable: true,  isFetching: false,teaserRequest:false};
          
         case ANALYZER_ERROR:
              return{...state,riskAnalyzer:false, teaserRequest:false};

         case ANALYZER_SUCCESS:
         
              return {...state,riskAnalyzer:true, teaserRequest:false};

        case 'RESET_TEASER':
         
              return {...state, TeaserList:action.payload, isAvailable: false, isFetching: false};

        case TEASER_REQUEST:
              return{ ...state, teaserRequest:true};

         default:
            return state;     
    }

};
 export default fetchVinTeaserReducer;