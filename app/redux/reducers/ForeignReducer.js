/**
 * Reducer updates and changes state with success report(LOCAL or FOREIGN) or error report
 */

import {REPORT_SUCCESS,REPORT_ERROR} from '../actions/types';



const initialState ={
   foreignReport:'',
   isFetching: true,
};


const ForeignReducer = (state = initialState, action) =>{
   
 
    switch(action.type){


         case REPORT_ERROR:
              return{...state, foreignReport:action.payload,};

         case REPORT_SUCCESS:
         
              return {...state,foreignReport:action.payload,};


         default:
            return state;     
    }

};
 export default ForeignReducer;