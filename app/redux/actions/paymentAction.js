
/**
 * Contains actions for the payment gateway 
 */



import {PAYMENT_SUCCESS,PAYMENT_ERROR,RESET,REPORT_SUCCESS,REPORT_ERROR} from './types';
import Expo from 'expo';
import {Actions} from 'react-native-router-flux';
import {ToastAndroid} from 'react-native';
import {FETCH_VIN_TEASER_SUCCESS,FETCH_VIN_TEASER_ERROR,ANALYZER_ERROR,ANALYZER_SUCCESS} from './types';




/**
 * Resets the state of reducer to initial state
 */
export const Reset = () => ({
  type: RESET,
  payload: '',
 });



 /**
  * @function ConfirmPayment
  * @param {string} json
  * Called when there is successful payment from the user
  * @return the new type and payload
  */

export const ConfirmPaymentSuccess = json => ({
     type:PAYMENT_SUCCESS,
     payload:json,
});



/**
 * 
 * @param {string} error 
 * Changes reducer state to error when user payment is unsuccessful
 * @returns new type and payload
 * 
 */
export const ConfirmPaymentError = error => ({
  type: PAYMENT_ERROR,
  payload: error,
});



/**
 * @param {string} json
 * Updates the reducer state when there is successful report generation
 * from API service
 */

export const ReportSuccess = json => ({
  type:REPORT_SUCCESS,
  payload:json,
});



/**
 * @param {string} error
 *Updates the reducer state when there is unsuccessful report generation
 * from API service
 */

export const ReportError = error => ({
type: REPORT_ERROR,
payload: error,
});





/**
 * Retrieves the payment reference from application memory
 * and confirms the payment from PAYSTACK 
 * @return response from PAYSTACK gateway
 */
export const ConfirmPayment = () => {
  return async dispatch =>{

    try{
       
           const ref = await Expo.SecureStore.getItemAsync('app')

           
          let response = await fetch('https://api.paystack.co/transaction/verify/'+ ref,{
              method:'GET',
              headers:{
              'content-type': 'application/json',
              'Authorization': 'Bearer '+ 'sk_live_9680b7fa1de308d5302ea8077769020a9b9deedf'
              },

          });
          
     let json = await response.json();
     
     if(json){
       
      dispatch(ConfirmPaymentSuccess(json.data.gateway_response));
    
     }

    }catch(error){
      dispatch(ConfirmPaymentError('Transaction Not Successful'));
      ToastAndroid.showWithGravity(
        'Network Error',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
    }
  

  };
 } ;

 
/**
 * @param {string} vin
 * Fetches local report on the given VIN
 * Retrives user token in memory and uses it for report generation
 */

 export const getLocal = vin => {
  return async dispatch =>{

    try{
       
      const value = await Expo.SecureStore.getItemAsync('user');
          let response = await fetch('https://api.carfacts.ng/api/local-vin/'+ vin,{
              method:'GET',
              headers:{
              'content-type': 'application/html',
              'Authorization': 'Bearer '+ value
              },

          });
          
     let json = await response.json();
     
     if(json){
       
      dispatch(ReportSuccess(json));
      Actions.ForeignReportScreen();
    
     }

    

    }catch(error){
       dispatch(ReportError('Errror in Downloading Report'));
       ToastAndroid.showWithGravity(
        'Network Error',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
    }
  

  };
 } ;



  
/**
 * @param {string} vin
 * Fetches Foreign report on the given VIN
 * Retrives user token in memory and uses it for report generation
 */

 export const getForeign = vin => {
  return async dispatch =>{

    try{
       
      const value = await Expo.SecureStore.getItemAsync('user');

          let response = await fetch('https://api.carfacts.ng/api/vin/'+ vin,{
              method:'GET',
              headers:{
              'content-type': 'application/html',
              'Authorization': 'Bearer '+ value
              },

          });
          
     let json = await response.text();
     
     if(json){
       
       dispatch(ReportSuccess(json));
       Actions.ForeignReportScreen();
    
     }

    }catch(error){
       dispatch(ReportError('Errror in Downloading Report'));
       ToastAndroid.showWithGravity(
        'Network Error',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
    }
  

  };
 } ;




 /**
  * @param {string} json
  * Updates the reducer state successful data retrieval
  * 
  */

export const fetchVinTeaserSuccess =json => ({
     type:FETCH_VIN_TEASER_SUCCESS,
     payload:json,
});




/**
  * @param {string} error
  * Updates the teaser reducer state on unsuccessful data retrieval
  * 
  */

export const fetchVinTeaserError = error => ({
  type: FETCH_VIN_TEASER_ERROR,
  payload: error,
});




/**
  * @param {string} json
  * Updates the analyzer reducer state on successful data retrieval
  * 
  */

export const AnalyzerSuccess =json => ({
  type:ANALYZER_SUCCESS,
  payload:json,
});




/**
  * @param {string} error
  * Updates the analyzer reducer state on unsuccessful data retrieval
  * 
  */

export const AnalyzerError = error => ({
type: ANALYZER_ERROR,
payload: error,
});




/**
 * 
 * @param {string} vin
 * @returns {JSON} containing vehicle details
 * year,model,engine type,country of manufacture etc 
 */
 export const fetchVinTeaser = (vin) =>{
    
  return async dispatch =>{
    try{

     let response = await fetch("https://api.carfacts.ng/api/vincheck/" + vin

    );
     let json = await response.text();
     const obj = JSON.parse(json);
     dispatch(fetchVinTeaserSuccess(obj));
     Actions.RiskAnalyzerScreen();
    }catch(error){
       dispatch(fetchVinTeaserError(error));
       ToastAndroid.showWithGravity(
        'Network Error',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
    }
   
  };
  
 } ;



 /**
  * Calculates risk of the given VIN/Chassis number
  * @param {string} vin
  * @returns "YES", "NO", "NOT AVAILABLE"
  */

 export const RiskAnalyzer = (vin) =>{

  return async dispatch =>{
    try{

     let response = await fetch("https://api.carfacts.ng/api/risk-analyzer/" + vin

    );
     let json = await response.text();
     const alert = JSON.parse(json);
    
     
     if(alert.RiskAlert === "YES"){
      dispatch(AnalyzerSuccess(true));
     }else if(json.RiskAlert === "NOT AVAILABLE"){
      ToastAndroid.showWithGravity(
        'Risk Analyzer not ',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
     }

    }catch(error){
       dispatch(AnalyzerError(false));
       ToastAndroid.showWithGravity(
        'Network Error',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
    }
  
  };
 } ;

