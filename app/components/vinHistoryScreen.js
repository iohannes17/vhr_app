import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Button} from 'react-native-elements';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'

import {
  StyleSheet,
  Text,
  ActivityIndicator,
  View,
  ScrollView
} from 'react-native';
import {fetchVinHistory } from '../redux/actions/fetchVinActions';
import {connect} from 'react-redux'
import CustomListview from './CustomList'
import {Toast} from 'native-base';



   class VinHistoryScreen extends Component{


 componentDidMount(){
  const id =  this.props.userDetails.userInfoList.user_id;
  this.props.fetchVinHistory(id);
 
 }

 componentWillUpdate(){
   this.displayChecks()
 }



 displayChecks =()=>{
  const toast = this.props.vinHist.vinhistoryList.total == undefined ?
   'Server error': this.props.vinHist.vinhistoryList.total;

  Toast.show({
    text:'Total VHR Checks :  '+ toast,
    buttonText:'',
    duration:1500
});
 }

 
    render() {
      let content = <CustomListview vins={this.props.vinHist.vinhistoryList.data} />;
      if (this.props.vinHist.isFetching) {
        content = 
                          <View style={styles.container}>
                               <ActivityIndicator size="large" />
                              </View>;
                            
      }
      return (
       
          <ScrollView contentContainerStyle={styles.container}>
          {content}
        
        </ScrollView>
         
      );
    }
  }

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    justifyContent:'center',
    height:hp('100%'),
    width:wp('100%')
  },
  indicator:{
    flex: 1, 
    justifyContent:'center',
    alignItems:'center',
    alignContent:'center',


  },
});




const mapStateToProps = state =>{
    return{
        vinHist: state.vinhistory,
        userDetails: state.userinfo,
       
    };
  }
  
  VinHistoryScreen.propTypes ={
    fetchVinHistory: PropTypes.func.isRequired,
    vinHist:PropTypes.any,
    userDetails: PropTypes.any,
    
  }
  
  
  export default connect(mapStateToProps, {fetchVinHistory})(VinHistoryScreen);