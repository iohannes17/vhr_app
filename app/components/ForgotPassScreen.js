import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert,
  ActivityIndicator
} from 'react-native';
import Wallpaper from './Wallpaper';
import {RetrievePassword} from '../redux/actions/registerActions';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {BarIndicator} from 'react-native-indicators';
import {heightPercentageToDP as hp,widthPercentageToDP as wp, widthPercentageToDP} from 'react-native-responsive-screen'




 class ForgotPasswordScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      
     
    }
  }

  recoverEmail = () => {
  
    const email = this.state.email;
    const frontend = 'https://staging.carfacts.ng/#/activate';
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;

     
    if(email.length < 0){Alert.alert('',this.props.regResponse.forgotPassSuccess); }
    else if(reg.test(email) === false){ Alert.alert('','Email is Incorrct')}
    else {

        const fPassword ={
        email: email ,
        frontend_url:frontend
      }
       this.props.RetrievePassword(fPassword);
     }
  }


  render() {
    return (
      <Wallpaper> 
        
        
  

             <View style={styles.container}>


               <Text style={styles.signUpText}>FORGOT PASSWORD</Text>
        
        <View style={styles.inputContainer}>
        <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/>
          <TextInput style={styles.inputs} 
              placeholder="Registered Email"
              secureTextEntry={false}
              onChangeText={(email) => this.setState({email})}
              value={this.state.email}
              underlineColorAndroid='transparent'/>
        </View>

        <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={this.recoverEmail}>
         
          {this.props.regResponse.isLoadingPassword ?  (<BarIndicator color='white' size={10} />) : <Text style={styles.BtnSignUpText}>RETRIEVE ACCOUNT</Text>}
        </TouchableHighlight>
      </View>
      </Wallpaper>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
   
  },
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:0,
      borderBottomWidth: 1,
      width: wp('90%'),
      height:50,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:180,
    borderRadius:30,
  },
  signupButton: {
    backgroundColor: '#fec005',
  },
  signUpText: {
    fontSize:14,
    fontWeight:"500",
    color:'#ffffff',
    marginBottom:20
  },
  BtnSignUpText: {
    fontSize:14,
    fontWeight:"500",
    color:'#ffffff',
  
  }
});


const mapStateToProps = state =>{
  return{
     regResponse: state.userreg
  };
}

ForgotPasswordScreen.propTypes ={
  RetrievePassword: PropTypes.func.isRequired,
  regResponse: PropTypes.object.isRequired
  
}



export default connect(mapStateToProps, {RetrievePassword})(ForgotPasswordScreen);