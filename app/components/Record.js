import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Dimensions from 'Dimensions';
import {Button} from 'react-native-elements';
import {StyleSheet, View,Text} from 'react-native';
import {Actions} from 'react-native-router-flux';

export default class Record extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>My Package</Text>
         
            <Text style={styles.text}>My Package</Text>
                   
      </View>
    );
  }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    top: 1,
    width: DEVICE_WIDTH,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  text: {
    color: 'white',
    backgroundColor: 'transparent',
    fontSize:10
  },

  textPackage: {
    color: '#339fe7',
    backgroundColor: 'transparent',
    fontWeight: 'bold',
    fontSize: 13,
    marginTop:6,
    alignSelf:'center'  
  },
});
