import { StyleSheet, Dimensions } from 'react-native';
const window = Dimensions.get('window');

export const IMAGE_HEIGHT = window.width /4;
export const IMAGE_HEIGHT_SMALL = window.width /7;
export const IMAGE_WIDTH= window.height/10;

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf:'center'
    
  },

  buttonLogin:{
    backgroundColor: '#fec005',
    flexDirection:'row',
    width:'30%',
    alignSelf:'center',
    marginLeft: 20,
    borderRadius: 25
},
  input: {
    height: 40,
    backgroundColor: 'transparent',
    marginHorizontal: 4,
    marginVertical: 5,
    borderRadius:0,
    fontSize:12,
    letterSpacing:3,
    alignItems:'center',
    justifyContent:'center',
    width: window.width - 30,
  },
  logo: {
    height:IMAGE_HEIGHT-20,
    resizeMode: 'contain',
    marginBottom: 10,
    padding:10,
    marginTop:20
  },

  paystackdownlogo: {
    height:80,
    resizeMode: 'contain',
    marginBottom: 10,
    marginTop:10,
    width:200
  },

  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius:0,
    borderColor:'#b6b6b6',
    borderBottomWidth: 1,
    width:window.width - 30,
    height:42,
    marginTop:10,
    marginBottom:10,
    flexDirection: 'row',
    alignItems:'center',
    elevation:2,
},


payinputContainer: {
  borderBottomColor: '#F5FCFF',
  backgroundColor: '#FFFFFF',
  borderColor:'#868686',
  borderBottomWidth: 1,
  width:window.width - 40,
  height:40,
  marginTop:10,
  marginBottom:10,
  marginBottom:5,
  flexDirection: 'row',
  alignItems:'center',
  elevation:1

},



yearpayinputContainer: {
  borderBottomColor: '#F5FCFF',
  backgroundColor: '#FFFFFF',
  borderRadius:0,
  borderColor:'#868686',
  borderBottomWidth: 1,
  width:window.width - 40,
  height:40,
  marginTop:10,
  marginBottom:10,
  marginBottom:5,
  flexDirection: 'row',
  alignItems:'center',
  elevation:1

},


cvvContainer: {
  borderBottomColor: '#F5FCFF',
  backgroundColor: '#FFFFFF',
  borderRadius:0,
  borderColor:'#868686',
  borderBottomWidth: 1,
  width:window.width - 40,
  height:40,
  marginTop:10,
  marginBottom:10,
  marginBottom:5,
  flexDirection: 'row',
  alignItems:'center',
  elevation:1
},

payContainer: {
  borderBottomColor: '#F5FCFF',
  backgroundColor: '#FFFFFF',
  borderRadius:0,
  borderBottomWidth: 1,
  width:window.width - 30,
  height:50,
  marginTop:10,
  marginBottom:10,
  marginBottom:5,
  flexDirection: 'column',
  alignItems:'center'
},

inputs:{
    height:40,
    marginLeft:16,
    borderBottomColor: '#FFFFFF',
    flex:1,
    
},
inputIcon:{
  width:30,
  height:30,
  marginLeft:15,
  marginRight:15
},

checkboxStyle:{
  marginTop:10,
  marginBottom:10, 
  alignSelf:'center'
},
  register:{
    marginBottom:40, 
    width:window.width -170,
    alignItems:'center',
    justifyContent:'center',
    height:40,
    borderRadius:0,
    backgroundColor: '#fec005',
},

paynow:{
  marginBottom:10, 
  width:window.width -180,
  alignItems:'center',
  justifyContent:'center',
  height:40,
  borderRadius:25,
  backgroundColor: '#fec005',
},

btnEye: {
    position: 'absolute',
    width: 43,
    height: 45,
    right: 15,
    top: 0,
  },

  iconEye: {
    width: 45,
    height: 45,
    tintColor: 'rgba(0,0,0,0.2)',
  },

  textstyle:{
    fontSize:30,
    fontWeight:"900",
    fontStyle:'normal',
    fontFamily:'Lato-Bold',
    color:'#ffffff',
    marginTop:30,
    marginBottom: 20
    
  },

  paytextstyle:{
    fontSize:18,
    fontWeight:"500",
    color:'#565656',
    marginBottom:30,
    marginTop:18,
    
  },

  innerpaytext:{
    fontSize:10,
    fontWeight:"500",
    color:'#b6b6b6',
    alignItems:'flex-start',
    padding:15,   
  },

  vat_text:{
    fontSize:10,
    fontWeight:"500",
    color:'#b6b6b6',
    alignSelf:'center',
    marginTop:10
     
  },

  pricetext:{
    marginBottom:13,
    fontSize:20,
    marginLeft: window.width - 170,
    fontWeight:"900",
    color:'green',
    alignItems:'center'

    
  },

  buttontext:{
    fontSize:20,
    fontWeight:"900",
    color:'#565656',
    alignItems:'flex-start'

    
  },

  payinput:{
    height: 40,
    backgroundColor: 'transparent',
    marginHorizontal: 2,
    marginVertical: 3,
    fontSize:10,
    fontWeight:"900",
    letterSpacing:4,
    fontStyle:'italic',
    alignItems:'center',
    justifyContent:'center',
    width: window.width - 30,
    alignItems:"center",
    
},

yearpayinput:{
  height: 40,
  backgroundColor: 'transparent',
  marginHorizontal: 2,
  marginVertical: 3,
  fontSize:10,
  fontWeight:"900",
  letterSpacing:2,
  alignItems:'center',
  justifyContent:'center',
  width: window.width - 300,
  alignItems:"center",
  
},

spinner:{
  color:'#fff',
},

dalert:{
  width: 400,
  height: '30%',
  alignContent:'center',
  alignItems:'center'
   
},

termsandConditionContainer:
{alignItems:'center'
,justifyContent:'center',
flexDirection:'row',
marginTop:20
},

TandCButton:
  {
    height:25,
    width:30,
    paddingRight:5,
    paddingLeft:5
  },

  TandCImage:
  {
    height:25,
    width:30,
    alignSelf:'center',
    tintColor:'#fff'
  },

  TandCtext:{
    color:'#fec005',
    fontWeight:"600",
    fontSize:12
  }


});

