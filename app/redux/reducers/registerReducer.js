/**
 * The reducer updates the state of user registration, password recovery
 */

import {REGISTER_REQUEST,REGISTER_SUCCESS,REGISTER_ERROR,FORGOT_PASSWORD_SUCCESS,FOGOT_PASSWORD_ERROR,PASSWORD_REQUEST} from '../actions/types';



const initialState ={
    isSuccess: false,
    isError: true,
    isLoading:true,
    forgotPass:false,
    forgotPassSuccess:'',
    isLoadingPassword:false
};


const registerReducer = (state = initialState, action) =>{

    switch(action.type){

        case REGISTER_REQUEST:
             return{ ...state, isLoading:true};

         case REGISTER_ERROR:
              return{ ...state, isLoading:false,isError:true,};

         case REGISTER_SUCCESS:
         
              return { ...state, isLoading:false,isSuccess:true,};

         case FORGOT_PASSWORD_SUCCESS:
              return{ ...state,forgotPass:true,forgotPassSuccess:'Email Recovery sent',isLoadingPassword:false};

         case FOGOT_PASSWORD_ERROR:
         
              return { ...state, forgotPass:false,forgotPassSuccess:'Error Sending Recovery Email',isLoadingPassword:false};     

          case PASSWORD_REQUEST:
              return{ ...state, isLoadingPassword:true};      

 
         default:
            return state;     
    }

};


export default registerReducer;