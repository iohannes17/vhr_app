import React, { Component } from "react";
import { StyleSheet, FlatList, Text, Image, View } from "react-native";
import PropTypes from "prop-types";


export default class TeaserListView extends Component {
  _keyExtractor = item => item.year;

  _renderItem = ({ item }) => {
    const {Vin, Year, Make, Model,Records,BodyStyle,EngineType,ManufacturedIn } = item;
    

    return (
      <View>
        <View style={styles.cardContainerStyle}>


          <View style={styles.row}>

           <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
              VIN/CHASSIS NUMBER   :
            </Text></View>
            <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
              {Vin}
            </Text></View>
          </View>


          <View style={styles.row}>

            <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
             Year   :
            </Text></View>
            <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
            {Year}
            </Text></View>
            </View>


                    <View style={styles.row}>

            <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
              Make  :
            </Text></View>
            <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
            {Make}
            </Text></View>
            </View>


                    <View style={styles.row}>

            <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
              Model :
            </Text></View>
            <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
            {Model}
            </Text></View>
            </View>


          <View style={styles.row}>

            <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
             BodyStyle  :
            </Text></View>
            <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
             {BodyStyle}
            </Text></View>
            </View>



          <View style={styles.row}>

                <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
                EngineType :
                </Text></View>
                <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
                {EngineType}
                </Text></View>
                </View>
                 
               <View style={styles.row}>

                <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
                 Plant Country :
                </Text></View>
                <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
                {ManufacturedIn}
                </Text></View>
                </View>


                  <View style={styles.row}>

                <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
                  Records :
                </Text></View>
                <View style={{width:'50%', height:'20%',alignContent:'center',justifyContent:'center'}}> <Text style={styles.cardTextStyle}>
                {Records}
                </Text></View>
                </View>


        </View>
      </View>
    );
  };

  render() {
    return (
      <FlatList
        style={{ flex: 1 }}
        data={this.props.vindetails}
        keyExtractor={this._keyExtractor}
        renderItem={this._renderItem}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
      />
    );
  }
}

TeaserListView.propTypes = {
  vindetails: PropTypes.string
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent"
  },
  cardContainerStyle: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    margin: 20,
    backgroundColor: "white",
    padding: 10,
    height:'60%'
  },
  faceImageStyle: {
    width: 65,
    height: 65
  },
  cardTextStyle: {
    color: "black",
    textAlign: "left"
  },
  row:{
      paddingTop:5,
      flex:1,
      flexDirection:'row',
  }
});