import React,{Component} from 'react';
import { Platform, StyleSheet,TouchableHighlight, View,TouchableNativeFeedback,Text,TouchableOpacity,Image} from 'react-native';
import PropTypes from 'prop-types';


import {
  Scene,
  Router,
  Actions,
  Reducer,
  ActionConst,
  Overlay,
  Tabs,
  Modal,
  Drawer,
  Stack,
  Lightbox,
} from 'react-native-router-flux';
import  Wallpaper from './Wallpaper';
import MaterialIntials from 'react-native-material-initials/native';
import {LinearGradient} from 'expo';
import {FetchUserInfo } from '../redux/actions/userInfoActions';
import {connect} from 'react-redux';
import Expo from 'expo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import {ResetVin} from '../redux/actions/vinTeaserActions';



class SideMenu extends Component {
    constructor(props) {
        super(props);

    };







  _logOut =  async () =>{

    this.props.ResetVin();
     await Expo.SecureStore.deleteItemAsync('user');
     await Expo.SecureStore.deleteItemAsync('price');
     await Expo.SecureStore.deleteItemAsync('type');
     await Expo.SecureStore.deleteItemAsync('packageType');
     await Expo.SecureStore.deleteItemAsync('month');
     Actions.LoginScreen();
   
        
  }



    render() {

    
        const fname =  this.props.userDetails.userInfoList.first_name;
        const lname =  this.props.userDetails.userInfoList.last_name;
        
        



        return (
            <Wallpaper>
            <View style={styles.container}>

                  <LinearGradient 
             colors={['rgba(0,0,0,0)','rgba(0,0,0,0)']}
           start={{x:0.0,y:1.0}}
           end={{x:1.0,y:1.0}}
           style={styles.tiles}>
        
           <MaterialIntials
            style={{alignSelf:'center',marginTop:10}}
            backgroundColor={'white'}
            color={'#fec005'}
            size={100}
            text={fname +" "+ lname}
            single={false}
           /> 
          </LinearGradient>    


            
           

            <View style ={styles.ButtonWrapper}>
            <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/home/ultraviolet/50/3498db'}}/>
            <TouchableOpacity style={styles.buttonStyle} onPress={Actions.DashBoardScreen}>
             <Text style={styles.ButtonLabel}>Home</Text>
            </TouchableOpacity>
            </View>

              
              <View style ={styles.ButtonWrapper}>
            <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/user/ultraviolet/50/3498db'}}/>
            <TouchableOpacity style={styles.buttonStyle} onPress={Actions.ProfileScreen}>
             <Text style={styles.ButtonLabel}>Edit Profile</Text>
            </TouchableOpacity>
            </View>

                <View style ={styles.ButtonWrapper}>
            <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/contact us/ultraviolet/50/3498db'}}/>
            <TouchableOpacity style={styles.buttonStyle} onPress={Actions.AboutUs}>
             <Text style={styles.ButtonLabel}>About Us</Text>
            </TouchableOpacity>
            </View>

           
           <View style ={styles.ButtonWrapper}>
            <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/>
            <TouchableOpacity style={styles.buttonStyle} onPress={Actions.ContactUs}>
             <Text style={styles.ButtonLabel}>Talk to Us</Text>
            </TouchableOpacity>
            </View>

             <View style ={styles.ButtonWrapper}>
            <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/friend/ultraviolet/50/3498db'}}/>
            <TouchableOpacity style={styles.buttonStyle}>
             <Text style={styles.ButtonLabel}>Refer a Friend</Text>
            </TouchableOpacity>
            </View>

             <View style ={styles.ButtonWrapper}>
            <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/circled question mark/ultraviolet/50/3498db'}}/>
            <TouchableOpacity style={styles.buttonStyle} onPress={Actions.Faq}>
             <Text style={styles.ButtonLabel}>FAQ ?</Text>
            </TouchableOpacity>
            </View>

           
           <View style ={styles.ButtonWrapper}>
            <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/logout/ultraviolet/50/3498db'}}/>
            <TouchableOpacity style={styles.buttonStyle} onPress={this._logOut}>
             <Text style={styles.ButtonLabel}>Log Out</Text>
            </TouchableOpacity>
            </View>
            

           
            </View>
            </Wallpaper>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: 'rgba(0,0,0,0)',
        paddingRight: 20,
        paddingLeft: 20,
        paddingBottom: 10,
    },

    tiles:{
        marginTop:hp('3'),
        height:'20%',
        width:'100%',
        alignItems:'center',
        justifyContent:'center',
        opacity:1,
        marginBottom:hp('2'),
      },

      inputIcon:{
        width:30,
        height:30,
        marginLeft:25,
        marginRight:15,
        top:8,
        left:10,
        position:'absolute',
        paddingLeft:5,
        paddingRight:5,
      },
      
    

   ButtonLabel:{
      color:'white',
      fontSize:15,
      fontFamily:'Lato-Regular',
      fontStyle:'normal',
      fontWeight:"900",
      textAlign:'center'
     
   },

   ButtonWrapper: {
    width: 300,
    height: 50,
    marginTop:hp('0.5%'),
    flexDirection:'row',
    justifyContent:'center',
    alignSelf:'center',
    backgroundColor:'transparent',
    borderBottomColor:'#e3f2fd',
    borderBottomWidth:0.5,
   
  
  },

  buttonStyle: {
    width: 150,
    height: 50,
    justifyContent:'center',
    alignSelf:'center',
    

    
  },

});

const mapStateToProps = state =>{
    return{
        userDetails: state.userinfo,
       
        
        
    };
  }
  
 SideMenu.propTypes ={
      FetchUserInfo: PropTypes.func.isRequired,
      ResetVin: PropTypes.func.isRequired,
      userDetails: PropTypes.any,
    
  }
  
  
  export default connect(mapStateToProps, {FetchUserInfo,ResetVin})(SideMenu);



 







  

// <Left>
// <Button transparent onPress={this._logOut}>
//   <Image source={require('../../assets/logout.png')} style={{width:25,height:25,tintColor:'#fff'}} />
//   </Button>
// </Left>

// <Right>
//   <Button transparent onPress={()=>Actions.ProfileScreen()}>
//   <Image source={require('../../assets/man.png')} style={{width:20,height:20,tintColor:'#fff',opacity:1}} />
//   </Button>
// </Right>