/**
 * Onboarding page for the mobile app
 * explains the application features of the program
 * @author: Owodiye John
 */


import {StyleSheet, View,ImageBackground,TouchableWithoutFeedback,Animated} from 'react-native';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome'
import React, {Component} from 'react';
import {IndicatorViewPager, PagerDotIndicator} from 'rn-viewpager';
import {Actions} from 'react-native-router-flux';
import {imageurl} from '../imageUrl'



const style = StyleSheet.create({
  container:{
    flex: 1,
    height:'100%',
  },
  backgroundImage:{
    flex: 1,
    position:'absolute',
    width:'100%',
    height:'100%',
    alignItems:'center',
    justifyContent: 'center'
  },
  buttonLogin:{
      backgroundColor: '#fec005',
      flexDirection:'row',
      width:'30%',
      alignSelf:'center',
      marginLeft: 20,
      borderRadius: 25
  },

  bottomView:{
      width:'100%',
      height:100,
      backgroundColor:'transparent',
      justifyContent:'center',
      position:'absolute',
      bottom:0
  }
      
});

export default class TransitionScreen extends Component {


    state={
        animatePress: new Animated.Value(1)
    }

    animateIn(){
        Animated.timing(this.state.animatePress,
            {
                toValue:0.5,
                duration:100
            }).start()
    }

    animateOut(){
        Animated.timing(this.state.animatePress,
            {
                toValue:1,
                duration:100
            }).start()
    }


    render() {
        return (
            <View style={style.container}>
                <IndicatorViewPager
                    style={style.container}
                    indicator={this._renderDotIndicator()} >

                    <View>
                        <ImageBackground source={{uri:imageurl+'1_jhcajz.png'}}  style={style.backgroundImage}>
                       
                        </ImageBackground>
                    </View>

                     <View>
                        <ImageBackground source={{uri:imageurl+'4.png'}}  style={style.backgroundImage}>

                           
                       </ImageBackground>
                    </View>

                     <View>
                        <ImageBackground source={{uri:imageurl+'2.png'}} style={style.backgroundImage}>
                        
                        
                        </ImageBackground>
                    </View>

                     <View>
                        <ImageBackground source={{uri:imageurl+'3.png'}}  style={style.backgroundImage}>


                        </ImageBackground>
                    </View>  
                </IndicatorViewPager>
                <TouchableWithoutFeedback  onPressIn={()=> this.animateIn()} onPressOut={()=>this.animateOut()}>
                            <Animated.View style={{
                                 width:'100%',
                                 height:100,
                                 backgroundColor:'transparent',
                                 justifyContent:'center',
                                 position:'absolute',
                                 bottom:0,
                                transform:[
                                    {
                                        scale:this.state.animatePress
                                    }
                                ]
                            }}>
                                <Button
                                        buttonStyle={style.buttonLogin}
                                        onPress = {Actions.LoginScreen}
                                        title='SKIP' />

                                </Animated.View>
                                
                                </TouchableWithoutFeedback>     
            </View>
        );
    }


    /**@this {_renderDotIndicator}
     * reyurns the number of dots for the pages in the onboard view
     */

    _renderDotIndicator() {
        return <PagerDotIndicator pageCount={4} />;
    }

    
}