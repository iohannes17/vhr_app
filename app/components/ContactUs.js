import React, {Component} from 'react';
import Dimensions from 'Dimensions';
import {View,StyleSheet,TouchableOpacity,TouchableHighlight,Image,Linking} from 'react-native';
import * as Animatable from 'react-native-animatable'
import { Container, Header, Content, List, ListItem, Text, Left, Body,Toast} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import call from 'react-native-phone-call';






 export default class ContactScreen extends Component {


  constructor(props){
    super(props);
    this.state ={
        fname:'',
        lname:'',
        package_active:'',
        transition:'lightSpeedIn'
    };
    
  }
  

  _Makecall =(phoneDetails) =>{
    call(phoneDetails)
         .catch(error =>{
          Toast.show({
            text:'Cannot open dialler at this time'
           })
         })
  }

 
  openUrl = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
       Toast.show({
        text:'Could not open' + url
       })
      }
    });
}



  
  render() {

  const args ={
    number:'07000227329',
    prompt:true
  };

  const instagramLink = 'https://instagram.com/carfaxng/';

  const twitterLink = 'https://twitter.com/carfaxng/';

  const facebookLink = 'https://facebook.com/carfaxng/';

  const linkedInLink = 'https://linkedin.com/in/carfacts-business-services-limited-902180182';

  const addressLink ='https://goo.gl/maps/dRJcJnunLUr';

     
    return (

      <Animatable.View style={styles.container} animation={this.state.transition}>
        <Container>
        <Content contentContainerStyle={{marginTop:20}}>
          <ListItem icon style={styles.liststyle}>
            <Left>
              <TouchableOpacity style={{width:30,height:30}} onPress ={()=>this._Makecall(args)}>
              <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/phone/339FE7'}}/>
              </TouchableOpacity>
            </Left>
            <Body>
              <Text style={styles.textStyle}>Phone Us</Text>
            </Body>
          </ListItem>
        
          <ListItem icon style={styles.liststyle}>
            <Left>
            <TouchableOpacity style={{width:30,height:30,}} onPress={()=>this.openUrl(instagramLink)}> 
            <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/instagram/339FE7'}}/>
              </TouchableOpacity>
            </Left>
            <Body>
              <Text style={styles.textStyle}>@carfaxng</Text>
            </Body>
          </ListItem>


            <ListItem icon style={styles.liststyle}>
            <Left>
            <TouchableOpacity style={{width:30,height:30,}} onPress={()=>this.openUrl(twitterLink)}>
              <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/twitter/339FE7'}}/>
              </TouchableOpacity>
            </Left>
            <Body>
              <Text style={styles.textStyle}>@carfaxng</Text>
            </Body>
          </ListItem>


          <ListItem icon style={styles.liststyle}>
            <Left>
            <TouchableOpacity style={{width:30,height:30,}}  onPress={()=>this.openUrl(facebookLink)}>
              <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/facebook/339FE7'}}/>
              </TouchableOpacity>
            </Left>
            <Body>
              <Text style={styles.textStyle}>carfaxng</Text>
            </Body>
          </ListItem>

            <ListItem icon style={styles.liststyle}>
            <Left>
            <TouchableOpacity style={{width:30,height:30,}}  onPress={()=>this.openUrl(linkedInLink)}>
              <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/linkedin/339FE7'}}/>
              </TouchableOpacity>
            </Left>
            <Body>
              <Text style={styles.textStyle}>Carfax Business Services Limited</Text>
            </Body>
          </ListItem>


            <ListItem icon style={styles.liststyle}>
            <Left>
            <TouchableOpacity style={{width:30,height:30,}}  onPress={()=>this.openUrl(addressLink)}>
              <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/address/339FE7'}}/>
              </TouchableOpacity>
            </Left>
            <Body>
              <Text style={styles.textStyle}>Number 270 Muritala Mohammed Road, Alagomeji Bustop, Yaba, Lagos</Text>
            </Body>
          </ListItem>
        </Content>
      </Container>
       </Animatable.View>
     
    );
  }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: DEVICE_WIDTH,
    flexDirection:'column',  
  },

  tiles:{
    height:'25%',
    width:'100%',
    alignItems:'center',
    justifyContent:'center',
    opacity:0.8
  },

  inputIcon:{
    width:30,
    height:30,
    justifyContent:'center'
  },

  liststyle:{
  marginTop:10,
  },
  
  textStyle:{

    fontFamily:'Lato-Regular'
  }   
  
});



    
  


