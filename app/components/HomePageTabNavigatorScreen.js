import React, { Component } from 'react';
import { Container, Header, Content, Footer, FooterTab, Button,Right,Left,Toast} from 'native-base';
import GetVhrScreen from './getVHRScreen';
import RiskAnalyzerScreen  from './riskanalyzer';
import VinHistoryScreen  from './vinHistoryScreen';
import {Image,StatusBar} from 'react-native';
import {fetchVinHistory } from '../redux/actions/fetchVinActions';
import {FetchUserInfo } from '../redux/actions/userInfoActions';
import {connect} from 'react-redux'
import {BackHandler,Text} from 'react-native'
import PropTypes from 'prop-types';
import {Actions,ActionConst} from 'react-native-router-flux';
import ShazamScreen from './CarShazam'
import {imageurl} from '../imageUrl'




 class FooterTabs extends Component {

    constructor(props){
        super(props);
        this.state ={
            index:0,
            elevation0:1,
            elevation1:0,
           elevation2:0,
           elevation3:0,
           
           activeBG0:'#ffffff',
            activeBG1:'rgba(0,0,0,0)',
            activeBG2:'rgba(0,0,0,0)',
            activeBG3:'rgba(0,0,0,0)',

            activeIconandText0:'#339FE7',
            activeIconandText1:'#fff',
            activeIconandText2:'#fff',
            activeIconandText3:'#fff',
           
        }

    }

    switchIndex = index =>{
        
        if(index == 0){
            this.setState({
                index:0,
                elevation0:1,
                elevation1:0,
                elevation2:0,
                elevation3:0,
               

                activeBG0:'#fff',
                activeBG1:'rgba(0,0,0,0)',
                activeBG2:'rgba(0,0,0,0)',
                activeBG3:'rgba(0,0,0,0)',
    
                activeIconandText0:'#339FE7',
                activeIconandText1:'#fff',
                activeIconandText2:'#fff',
                activeIconandText3:'#fff',
             })
        }
        else if(index == 1){
            this.setState({
                index:1,

                elevation1:1,
                elevation0:0,
                elevation2:0,
                elevation3:0,
              

                activeBG1:'#fff',
                activeBG0:'rgba(0,0,0,0)',
                activeBG2:'rgba(0,0,0,0)',
                activeBG3:'rgba(0,0,0,0)',
    
                activeIconandText1:'#339FE7',
                activeIconandText0:'#fff',
                activeIconandText2:'#fff',
                activeIconandText3:'#fff',
                
            })
        }
       
        else if(index == 2){
                this.setState({
                    index:2,

                    elevation2:1,
                    elevation1:0,
                    elevation0:0,
                    elevation3:0,
                 
                    activeBG2:'#fff',
                    activeBG1:'rgba(0,0,0,0)',
                    activeBG3:'rgba(0,0,0,0)',
                    activeBG0:'rgba(0,0,0,0)',
        
                    activeIconandText2:'#339FE7',
                    activeIconandText1:'#fff',
                    activeIconandText3:'#fff',
                    activeIconandText0:'#fff',

                    
                });

                const id =  this.props.userDetails.userInfoList.user_id;
                this.props.fetchVinHistory(id);
           }
           
           
           else if(index == 3){
            this.setState({
                index:3,

                elevation3:1,
                elevation0:0,
                elevation2:0,
                elevation1:0,
              

                activeBG3:'#fff',
                activeBG0:'rgba(0,0,0,0)',
                activeBG1:'rgba(0,0,0,0)',
                activeBG2:'rgba(0,0,0,0)',
    
                activeIconandText3:'#339FE7',
                activeIconandText0:'#fff',
                activeIconandText1:'#fff',
                activeIconandText2:'#fff',
                
            })
        }
        
    }

    async componentDidMount(){
        this.props.FetchUserInfo();
        BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
    
      }


      componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);   
        
      }
      
      handleBackButton(){
          return true;
      }

      




  render() {
 

    let AppIndex;
    const index = this.state.index;

    


    if(index == 0){

        AppIndex = <GetVhrScreen/>
      
    }

    else if(index == 1){
       AppIndex = <RiskAnalyzerScreen/>
    }

    else if(index == 2){
        AppIndex =  <VinHistoryScreen/>
     }

    else if(index == 3){
        AppIndex = <ShazamScreen/>
    
    }
    



    return (
      <Container>
            <StatusBar hidden={true} />
        {/* <Header/> */}
        <Content>               
            {AppIndex}
        </Content>
        <Footer >
          <FooterTab style={{backgroundColor:'rgba(3, 169, 244, 0.7)' }}>

            <Button transparent badge vertical onPress={()=>this.switchIndex(0)}
             style={{
                backgroundColor:this.state.activeBG0,
                elevation:this.state.elevation0}}>
            <Image source={{uri:imageurl+'purchase_vhr2.png'}} style={{width:30,height:30,}} /> 
              <Text style={{color:this.state.activeIconandText0,fontSize:10}}>Car Records</Text>
            </Button>


            <Button vertical onPress={()=>this.switchIndex(1)} 
            style={{
                backgroundColor:this.state.activeBG1,
                elevation:this.state.elevation1}}>
            <Image source={{uri:imageurl+'analyze.png'}} style={{width:30,height:30,tintColor:this.state.activeIconandText1}} /> 
              <Text style={{color:this.state.activeIconandText1,fontSize:10}}>Risk Analyzer</Text>
            </Button>


             <Button vertical onPress={()=>this.switchIndex(2)}
              style={{
                backgroundColor:this.state.activeBG2,
              elevation:this.state.elevation2}}>
               <Image source={{uri:imageurl+'analytics.png'}} style={{width:29,height:29,tintColor:this.state.activeIconandText2}} />  
              <Text style={{color:this.state.activeIconandText2,fontSize:10}}>Recent History</Text>
            </Button>
           
            <Button vertical onPress={()=>this.switchIndex(3)}
              style={{
                backgroundColor:this.state.activeBG3,
              elevation:this.state.elevation3}}>
               <Image source={{uri: 'https://png.icons8.com/car/ffffff'}} style={{width:29,height:29,tintColor:this.state.activeIconandText3}} />  
              <Text style={{color:this.state.activeIconandText3,fontSize:10}}>Car Shazam</Text>
            </Button>


           

          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const mapStateToProps = state =>{
    return{
        userDetails: state.userinfo,
    };
  }

FooterTabs.propTypes ={
    fetchVinHistory: PropTypes.func.isRequired,
    FetchUserInfo: PropTypes.func.isRequired,
    userDetails: PropTypes.any,
  
    
    
  }
  
  
  export default connect(mapStateToProps, {fetchVinHistory,FetchUserInfo})(FooterTabs);