/**
 * @class RiskAnalyzer
 * It analyzes VIN/Chassis number of any vehicle
 * 
 */





import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  ActivityIndicator,
  Dimensions,
  ScrollView,
} from 'react-native';
import {Toast} from 'native-base';
import Wallpaper from './Wallpaper';
import {connect} from 'react-redux'
import PropTypes from 'prop-types';
import {Card} from 'react-native-elements';
import Expo from 'expo';
import {Actions} from 'react-native-router-flux';
import {imageurl} from '../imageUrl'




 class RiskAnalyzerScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      vin: '',
     

    }
    this._onPress = this._onPress.bind(this);
    this._getVHR = this._getVHR.bind(this);
   
  }
  

  /**
   * @this {_onPress}
   * @param {numer} price
   * @param {string} type
   * @param {string} packageT
   * 
   * saves the price,the amount of vhr and package type(loacal or foreign)
   * It transitions to the payment gateway  
   */
  _onPress = async (price,type,packageT) => {
     
    const vinNumber = this.state.vin;
    if(vinNumber === ''){
      Toast.show({
        text: 'Empty VIN',
        buttonText: 'Enter a VIN/Chassis Number'
      })
    }
    else if(vinNumber.lemgth < 17){
      Toast.show({
        text: 'VIN too short',
        buttonText: 'Enter a 17 character VIN/Chassis Number'
      })
  
    }else{
      await Expo.SecureStore.setItemAsync('vin_number',this.state.vin);
    await Expo.SecureStore.setItemAsync('price',price)
    await Expo.SecureStore.setItemAsync('type',type)
    await Expo.SecureStore.setItemAsync('packageType',packageT)
    Actions.Payment();
    }
   
  
  }
  


 /**
  * saves the vin/Chassis number in memory and transitions to the 
  * screen where user purchases VHR
  */
 async _getVHR(){
   const vinNumber = this.state.vin;
    await Expo.SecureStore.setItemAsync('vin_number',vinNumber);

    Actions.PurchaseVHRScreen();
 }


 /**
  * Deletes the vin Number saved in app memory as the component unmounts
  */
 async componentWillMount(){
  await Expo.SecureStore.deleteItemAsync('vin_number');
 }





  render() {
     
   /** Toggles element rendering depending on state change*/ let content;

      if (!this.props.teaser.isAvailable) {
        content = 
                    <View style={{marginTop:30}}>
                          <ActivityIndicator size="large" />
                        </View>;                          
      }
      else{

        content = <View style={{alignItems:'center',alignContent:'center'}}>
                  
           
            <Text style={styles.textteaser}>Teaser Details</Text>

                <Card containerStyle={{width:300,height:40,backgroundColor:'white',}}>
                   <Text style={styles.attStyle}>VIN : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <Text style={styles.propStyle}>{this.props.teaser.TeaserList.VIN}
                 </Text></Text>              
            </Card>

             <Card containerStyle={{width:300,height:40,backgroundColor:'white',}}>
                   <Text style={styles.attStyle}>Make : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <Text style={styles.propStyle}>{this.props.teaser.TeaserList.Make}
                 </Text></Text>              
            </Card>

              <Card containerStyle={{width:300,height:40,backgroundColor:'white',}}>
                   <Text style={styles.attStyle}>Model : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <Text style={styles.propStyle}>{this.props.teaser.TeaserList.Model}
                 </Text></Text>              
            </Card>

            <Card containerStyle={{width:300,height:40,backgroundColor:'white',}}>
                   <Text style={styles.attStyle}>Body Style : &nbsp;&nbsp;
                     <Text style={{color:'#000000',fontSize:10}}>{this.props.teaser.TeaserList.BodyStyle}
                 </Text></Text>              
            </Card>


            <Card containerStyle={{width:300,height:40,backgroundColor:'white',}}>
                   <Text style={styles.attStyle}>Engine Style : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <Text style={styles.propStyle}>{this.props.teaser.TeaserList.EngineType}
                 </Text></Text>              
            </Card>
            <Card containerStyle={{width:300,height:40,backgroundColor:'white',}}>
                   <Text style={styles.attStyle}>Plant Country : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <Text style={styles.propStyle}>{this.props.teaser.TeaserList.ManufacturedIn}
                 </Text></Text>              
            </Card>

            <Card containerStyle={{width:300,height:40,backgroundColor:'white',}}>
                   <Text style={styles.attStyle}>Record : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <Text style={styles.propStyle}>{this.props.teaser.TeaserList.Records}
                 </Text></Text>              
            </Card>

             
             {this.props.teaser.riskAnalyzer &&  <View style={{width:300, backgroundColor:'black',marginTop:30,marginBottom:20}}>
            <Image source={{uri:imageurl+'risk.png'}}/> style={{alignSelf:'center',height:109,marginTop:28,marginBottom:20,width:95}}/>
            </View>}


             <View styles={styles.btn}>
            <TouchableHighlight style={{alignSelf:'center',
              height:35,   
              marginTop:10,
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom:20,
              width:150,
              borderRadius:25,
              backgroundColor:'gold',
          

          }}
           onPress={this._getVHR}>

              <Text style={styles.signUpText}>Get VHR</Text>
            </TouchableHighlight>
            </View>

           
            </View>;

      }





    return (
     
      <Wallpaper>
      <ScrollView scrollEnabled={true}  style={{height:DEVICE_HEIGHT -100}}>
     

         <View style={{width:DEVICE_WIDTH,height:150, justifyContent:'center',alignContent:'center',marginBottom:20,marginTop:15}}>
         <Image source={{uri:imageurl+'purchase_vhr2.png'}} style={{width:170,height:150, alignSelf:'center'}} />  
         </View>

          
      
         
          <View style={styles.tiles2}>
          <TextInput style={styles.inputs}
              placeholder="VIN/Chassis Number"
              underlineColorAndroid='transparent'
              value={this.state.vin}
              returnKeyType={'done'}
              autoCorrect={false}
              onChangeText={(vin) => this.setState({vin})}/>

            <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={()=>this._onPress('1000','1','riskanalyzer')}>
          <Text style={styles.signUpText}> &nbsp;Analyze Vin </Text>
        </TouchableHighlight>
            </View>   
              
              <View style={styles.tiles4}> 
              {content}
              </View>
         
      </ScrollView>   
      </Wallpaper>
    

    );
  }
}


const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: DEVICE_WIDTH,
    flexDirection:'row',  
   
  },

  tiles:{
    height:'7%',
    width:'100%',
    alignItems:'center',
    justifyContent:'center',
    marginTop:2
  },
  inputContainer: {
      marginTop:40,
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:350,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      borderBottomColor: '#FFFFFF',
      backgroundColor:'white',
      borderRadius:15,
      width:'90%',
      marginBottom:15,
      marginTop:1,
      fontSize:25,
      fontWeight:'500',
      shadowOpacity:0.5,
      elevation:10,
      paddingLeft:25,
      paddingRight:7,
      letterSpacing:1,
     
  },
 
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },

  signupButton: {
    backgroundColor: "#fec005",
  },
  signUpText: {
    color: 'white',
  },
  tiles2: {
    height:' 2%',
    width:'100%',
    marginBottom:15,
    marginTop:30,
    alignItems:'center',
    justifyContent:'center'

   
  },
 
  text2: {
    color: 'gold',
    backgroundColor: 'transparent',
    fontSize: 20,
    marginTop:2,
    alignSelf:'center',
    marginBottom:25,
    fontFamily:'System',
    fontWeight:'bold'
  },

  textteaser: {
    color: 'gold',
    backgroundColor: 'transparent',
    fontSize: 20,
    fontWeight:"500",
    alignSelf:'center',
    marginBottom:2,
    marginTop:10,
  },

  tiles4: {
    height:'87%',
   width:'100%',
   alignItems:'center',
   flexDirection:'column',
   marginTop:20,
   
 },

btn: {
  height:' 3%',
  width:'100%',
  marginBottom:20,
  alignItems:'center',
  justifyContent:'center',
  marginLeft: 60,
},


 Accstyle: {
  height:'13%',
  width:'100%',
 flexDirection:'column',
},


attStyle:{
   fontSize: 12,
   fontWeight:"500",
   color:'#000000',
   marginBottom:15
},

propStyle:{
  fontSize: 14,
  fontWeight:"900",
  color:'#000000',
},

dalert:{
  width: 400,
  height: '30%',
  alignContent:'center',
  alignItems:'center'
   
},

});



const mapStateToProps = state =>{
  return{
      teaser: state.vinteaser,
  };
}


RiskAnalyzerScreen.propTypes ={
  teaser:PropTypes.any.isRequired,
  userinfo:PropTypes.any,
}


export default connect(mapStateToProps, {})(RiskAnalyzerScreen);