
import {ACTIVE_PLAN_SUCCESS,ACTIVE_PLAN_ERROR,CANCEL_PLAN} from './types';
import Expo from 'expo';
import {Alert} from 'react-native';



/**
 * @param {JSON} json
 * 
 */

export const ActivePlanSuccess = json => ({
     type:ACTIVE_PLAN_SUCCESS,
     payload:json,
});




/**
 * @param {String} error
 * 
 */
export const ActivePlanError = error => ({
  type: ACTIVE_PLAN_ERROR,
  payload: error,
});





/**
 * @function FetchActivePlan
 * Retrieves logged in user active plan
 * @return {JSON} 
 */
  export const FetchActivePlan = () =>{
    
    return async dispatch =>{

      try{
         
          const value = await Expo.SecureStore.getItemAsync('user');
          
            let response = await fetch('https://api.carfacts.ng/api/userplan',{
                method:'GET',
                headers:{
                'content-type': 'application/json',
                'Authorization': 'Bearer '+ value
                },
            });
            
       let json = await response.json();
       
       if(json.user_plan){

        dispatch(ActivePlanSuccess(json.user_plan));
      
       }

      }catch(error){
        dispatch(ActivePlanError(error));
      }
    
    };
   } ;


