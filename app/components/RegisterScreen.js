/**
 * Second form page for the user registration 
 * individual signup details
 * password
 * address
 * city
 * state
 * 
 */



import React, { Component } from 'react';
import { View, TextInput,Text, ScrollView,Image, Animated,TouchableOpacity, Keyboard, KeyboardAvoidingView,Platform,Alert,Linking,WebView} from 'react-native';
import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL } from './styles';
import Wallpaper from './Wallpaper';
import {Actions} from 'react-native-router-flux';
import {RegisterUser} from '../redux/actions/registerActions';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {BarIndicator} from 'react-native-indicators';
import Expo from 'expo';
import {imageurl} from '../imageUrl'



class RegisterScreen extends Component {



  /**
   * @constructor
   * initializes the state of each variable
   * 
   */

  constructor(props) {
    super(props);
    this.state = {
      password: '',
      cpassword:'',
      address:'',
      city:'',
      statee:'',
      isLoading:false,
      showpass:true,
      press:false,
      terms_condition:false
    }
    this._onRegister = this._onRegister.bind(this);
    this.showPass = this.showPass.bind(this);
    this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
   
  }


  /**
   * selectively displays the animations based the platform 
   * the app runs
   * 
   */

  componentWillMount () {
    if (Platform.OS=='ios'){
     this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
     this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
    }else{
     this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
     this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
    }
 
   }


   /**
    * Calls the keyboard methods when the component is unmounted
    */
 
   componentWillUnmount() {
     this.keyboardWillShowSub.remove();
     this.keyboardWillHideSub.remove();
     this.setState({terms_condition:false});
   }


   /**
    * @this {keyboardWillShow}
    * Changes the image size when the keyboard shows on screen
    */
 
   keyboardWillShow = (event) => {
     Animated.timing(this.imageHeight, {
       duration: event.duration,
       toValue: IMAGE_HEIGHT_SMALL,
     }).start();
   };


   /**
    * @this {keyboardWillHide}
    * Changes the image height when keyboard is hidden
    */
 
   keyboardWillHide = (event) => {
     Animated.timing(this.imageHeight, {
       duration: event.duration,
       toValue: IMAGE_HEIGHT,
     }).start();
   };
 

   /**
    * @this {keyboardDidShow}Toggles the visibility of the keyboard
    * when the image height is small
    */
 
   keyboardDidShow = (event) => {
     Animated.timing(this.imageHeight, {
       toValue: IMAGE_HEIGHT_SMALL,
     }).start();
   };
 

   /**
    * @this {keyboardDidHide}
    * Hides the keyboard when the icon is large
    */
   keyboardDidHide = (event) => {
     Animated.timing(this.imageHeight, {
       toValue: IMAGE_HEIGHT,
     }).start();
   };



   /**
    * @this {showpass}
    * Toggles the visibility of the password entered by user
    */
  showPass() {
    this.state.press === false
      ? this.setState({showpass: false, press: true})
      : this.setState({showpass: true, press: false});
  }
 

  /**
   * @this {_onRegister}
   * Registers new details using the user details
   * It collects the user details saved in memory
   * and the new user details ans registers the user
   */


  async _onRegister() {
     
    if(this.state.cpassword.length < 8 || this.state.password.length < 8 ){
        
      Alert.alert(
        '',
        'Password must be minimum of 8 characters'
      );
    }
    else if(this.state.cpassword !== this.state.password){
         
      Alert.alert(
        '',
        'Passwords must match'
      );
 
    }

    else if(this.state.city.length < 0 || this.state.statee.length <0 || this.state.address.length < 0){
         
      Alert.alert(
        '',
        'please fill all fields'
      );
 
    }

    else if(this.state.terms_condition === false){
      Alert.alert(
        '',
        'Please Accept Terms and Conditions'
      );

    }
    else{

      const username = await Expo.SecureStore.getItemAsync('username');
     const fname = await Expo.SecureStore.getItemAsync('fname',fname);
      const lname = await Expo.SecureStore.getItemAsync('lname',lname);
     const email = await Expo.SecureStore.getItemAsync('email',email);
     const phone = await Expo.SecureStore.getItemAsync('phone',phone);
     
     const registerParam ={
       username: username,
       email: email ,
       first_name: fname,
       last_name: lname,
       phone: phone,
       password: this.state.password,
       password_confirmation: this.state.cpassword,
       address_line_1:this.state.address,
       city:this.state.city,
       state:this.state.statee,
       frontend_url:'https://staging.carfacts.ng/#/activate'
     }
      this.props.RegisterUser(registerParam);
      this.setState({isLoading:this.props.regResponse.isLoading});

    
      
    }
  }


  checkTerms = () =>{
    this.setState({
      terms_condition:true
    });
  }

  uncheckTerms = () =>{
    this.setState({
      terms_condition:false
    });
  }

  openUrl = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        Alert.alert(
          '',
          'Cannot open  ' + url
        );
      }
    });
}






  render() {
     

    const url = 'https://www.carfax.ng/#/terms'
 
    return (
    
<Wallpaper>
        <View style={{flex:1,alignItems:'center'}}>
        
        <ScrollView style={{flex:1,marginTop:40}} scrollEnabled={true} showsHorizontalScrollIndicator={true}>

          <KeyboardAvoidingView
          style={styles.container}
          behavior="padding"
        >

        <View style={styles.inputContainer}>
        <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/address/ultraviolet/50/3498db'}}/>
        <TextInput
              placeholder="Address"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              value ={this.state.address}
              onChangeText={(address) => this.setState({address})}
              style={styles.input}
            />
          </View>

          <View style={styles.inputContainer}>
        <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/city/ultraviolet/50/3498db'}}/>
        <TextInput
              placeholder="City"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              value ={this.state.city}
              onChangeText={(city) => this.setState({city})}
              style={styles.input}
            />
          </View>

            <View style={styles.inputContainer}>
        <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/state/ultraviolet/50/3498db'}}/>
        <TextInput
              placeholder="State"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              value ={this.state.statee}
              onChangeText={(statee) => this.setState({statee})}
              style={styles.input}
            />
          </View>

          <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/password/ultraviolet/50/3498db'}}/>
        <TextInput
              secureTextEntry ={this.state.showpass}
              placeholder="Password"
              underlineColorAndroid='transparent'
              value ={this.state.password}
              keyboardAppearance={'dark'}
              autoCorrect={false}
              returnKeyType={'done'}
              onChangeText={(password) => this.setState({password})}
              style={styles.input}
            />

            <TouchableOpacity
                  activeOpacity={0.8}
                  style={styles.btnEye}
                  onPress={this.showPass}>
                  <Image source={{uri:imageurl+'eye_black.png'}} style={styles.iconEye} />
                </TouchableOpacity>
          </View>

              <View style={styles.inputContainer}>
              <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/password/ultraviolet/50/3498db'}}/>
                <TextInput
                      secureTextEntry ={this.state.showpass}
                      placeholder="Confirm Password"
                      underlineColorAndroid='transparent'
                      value ={this.state.cpassword}
                      keyboardAppearance={'dark'}
                      autoCorrect={false}
                      returnKeyType={'done'}
                      onChangeText={(cpassword) => this.setState({cpassword})}
                      style={styles.input}
                    />

                    <TouchableOpacity
                  activeOpacity={0.8}
                  style={styles.btnEye}
                  onPress={this.showPass}>
                  <Image sources={{uri:imageurl+'eye_black.png'}} style={styles.iconEye} />
                </TouchableOpacity>
          </View>

            
        </KeyboardAvoidingView>
        <View style={styles.termsandConditionContainer}>

         {this.state.terms_condition  ?
                    <TouchableOpacity style={styles.TandCButton} onPress={this.uncheckTerms}>
                    <Image source={{uri:imageurl+'check.png'}} style={styles.TandCImage}/>         
                    </TouchableOpacity> :

                    <TouchableOpacity style={styles.TandCButton} onPress={this.checkTerms}>
                    <Image source={{uri:imageurl+'unchecked.png'}} style={styles.TandCImage}/>         
                    </TouchableOpacity>
        }
                <Text style={styles.TandCtext} 
                  onPress={()=>this.openUrl(url)}> 
                      Agree to Terms and Conditions</Text>
          </View>
        </ScrollView>
         
          <View>
            <TouchableOpacity style={styles.register} onPress={this._onRegister}>
            {this.state.isLoading ?  (<BarIndicator color='white' size={10} />):( <Text style={styles.signUpText}>CREATE ACCOUNT</Text>)  }
                    
            </TouchableOpacity>
          </View>
        </View>
</Wallpaper>

    );
  }
}

const mapStateToProps = state =>{
  return{
     regResponse: state.userreg
  };
}

RegisterScreen.propTypes ={
  RegisterUser: PropTypes.func.isRequired,
  regResponse: PropTypes.object.isRequired
  
}



export default connect(mapStateToProps, {RegisterUser})(RegisterScreen);