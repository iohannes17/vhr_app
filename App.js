import React,{Component} from 'react';
import { StyleSheet, View, TouchableOpacity,Image,BackHandler,Alert,Text} from 'react-native';
import LoginScreen from './app/components/LoginScreen';
import RegisterScreen from './app/components/RegisterScreen';
import RegisterScreenTwo from './app/components/RegisterScreenInit';
import ForgotPasswordScreen from './app/components/ForgotPassScreen';
import TransitionScreen from './app/components/TransitionScreen';
import VinHistoryScreen from './app/components/vinHistoryScreen';
import GetVhrScreen from './app/components/getVHRScreen';
import Paystack from './app/components/paystack';
import ForeignReport from './app/components/foreignReport';
import RenewSubscriptionScreen from './app/components/renewSubScreen';
import VerifyPay from './app/components/verifyPayment';
import RiskAnalyzerScreen from './app/components/riskanalyzer';
import ProfileScreen from './app/components/ProfileScreen';
import SideMenu from './app/components/SideMenu';
import AboutUsScreen from './app/components/AboutUs';
import ContactScreen  from './app/components/ContactUs';
import  NotificationScreen from './app/components/Notifiations';
import Faq from './app/components/FaqScreen';
import ShazamScreen from './app/components/CarShazam'
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import {imageurl} from './app/imageUrl';

import { Router, Scene, Actions, ActionConst,Stack,Drawer} from 'react-native-router-flux';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import rootReducers from './app/redux/reducers/rootReducer';
import {Font,Asset} from 'expo';
import { Platform, StatusBar,StatusBarIOS } from 'react-native';
import RNPaystack from 'react-native-paystack';
import FooterTabs from './app/components/HomePageTabNavigatorScreen'
import {Root} from 'native-base'
import Expo from 'expo';











const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const store = createStoreWithMiddleware(rootReducers);


export default class App extends Component {



	constructor(props) {
    super(props);
    this.state = { 
			fontsReady:false,
			onEnter:false,
			isReady:false
		};
	
  }

async componentDidMount(){
	BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
	 
  this.initPaystack()

}


initPaystack(){
	try{
		RNPaystack.init({publicKey:'pk_live_649decac0c04697cb97eceb56d756c7f9e1068b4'});
	}catch(error){
	
	}
}

componentWillUnmount(){
	BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
	
}

handleBackButton(){
	Alert.alert(
		'Exit App',
		'Do you want to close the App',
		[
			{text:'No',style:'cancel'},
			{text:'Yes',onPress:()=> BackHandler.exitApp()},
		],
		{cancelable:false});
		return true
   
}


backbutton = async () =>{
	Actions.pop();

}



async initProjectFonts() {

	await Font.loadAsync({
		"Lato-Regular": require("./assets/fonts/LatoRegular.ttf"),
		"Lato-Bold": require("./assets/fonts/LatoBold.ttf"),
		"Spectral-ExLight":require("./assets/fonts/Spectral-ExtraLightItalic.ttf"),
		"Spectral-SemiBold": require("./assets/fonts/Spectral-SemiBold.ttf"),
		Roboto: require("native-base/Fonts/Roboto.ttf"),
		Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
	
	})
	
	await Expo.SecureStore.deleteItemAsync('user');
	
}




onEnter=()=>{
	this.setState({
		onEnter:true
	})
}

onExit=()=>{
	this.setState({
		onEnter:false
	})
}



  render() {


    const styles = StyleSheet.create({
      container:{
        flex:2,
			},
			navbar:{
				backgroundColor:'#339FE7',
				height:40,
			
			}
		});
		

		const renderBackButton = () => (								
           <TouchableOpacity
					 onPress={this.backbutton}>
            <View style={{alignItems:'center'}}>
						<Image 
						    style={{width:20,height:20,marginLeft:13}}
							source={{uri:imageurl+'left-arrow.png'}}/>
						</View>
     </TouchableOpacity>
		);

		const renderBackButton2 = () => (
			<TouchableOpacity
			onPress={() => Actions.DashBoardScreen()}>
			 <View style={{alignItems:'center'}}>
			 <Image 
					 style={{width:20,height:20,marginLeft:13}}
					 source={{uri:imageurl+'left-arrow.png'}}/>
			 </View>
</TouchableOpacity>
);


const renderLeftDrawerButton= () => (
	<TouchableOpacity
	  onPress={Actions.Notification}>
	 <View style={{alignItems:'center'}}>
	 <Image style={{width:25,height:25,marginRight:10,}} source={{uri:'https://png.icons8.com/Notification/ffffff/96'}}/>
	 </View>
</TouchableOpacity>);


if (!this.state.isReady) {
	return <Expo.AppLoading 
			startAsync={()=> this.initProjectFonts()}
			onFinish={()=> this.setState({isReady:true})}
	/>;

}

    return(
			<Provider store={store}>
	 <View style={styles.container}>
	 <Root>

	 { Platform.OS === 'android' && Platform.Version >= 20 ? 
			  <StatusBar hidden={true} />: 
			<StatusBarIOS/> }
        


           
     
			
        <Router>  
	      <Scene key="root">
	        <Scene key="TransitionScreen"
	          component={TransitionScreen}
	        	animation='fade'
						hideNavBar={true}	
						initial ={true}
					
					
				 />

	        <Scene key="LoginScreen"
	          component={LoginScreen}
	          animation='fade'
						hideNavBar={true}
					
						
	        />
          <Scene key="ForgotPasswordScreen"
	          component={ForgotPasswordScreen}
						animation='fade'
						navigationBarStyle={styles.navbar}
						backButtonBarStyle={{tintColor:'white'}}
					  renderBackButton ={() => renderBackButton()}
						hideNavBar={false}
				  />

          <Scene key="RegisterScreenTwo"
	          component={RegisterScreenTwo}
	          animation='fade'
						hideNavBar={false}
						navigationBarStyle={styles.navbar}
						renderBackButton ={() => renderBackButton()}
				 />
	       <Scene key="RegisterScreen"
	          component={RegisterScreen}
	          animation='fade'
						hideNavBar={false}
						navigationBarStyle={styles.navbar}
						renderBackButton ={() => renderBackButton()}
				 />
         


          <Drawer
					 hideNavBar ={false}
					 navigationBarStyle={styles.navbar}
					 Key ="drawerMenu"
					 drawerPosition="left"
					 drawerWidth ={250}
					 contentComponent={SideMenu}
					 drawerIcon={
					 <Image style={{tintColor:'#fff',width:wp('7.5%'),height:hp('3.8%'),resizeMode:'contain'}} 
					 source={{uri:imageurl+'menu.png'}}/>}
					 renderTitle={<Text style={{color:'white',fontFamily:'Lato-Bold'}}>Dna4Wheels</Text>}
					 renderRightButton ={()=> renderLeftDrawerButton()}>

	
					 <Scene key="DashBoardScreen"
	          component={FooterTabs}
						animation='fade'
						hideNavBar={true}/>


						 <Scene key="Vhr"
	          component={GetVhrScreen}
						animation='fade'
						navigationBarStyle={styles.navbar}
						hideNavBar={true}
					
					
	        />

						 <Scene key="VinHistory"
	          component={VinHistoryScreen}
						animation='fade'
						hideNavBar={true}
						navigationBarStyle={styles.navbar} 
					
					  
					  
	        />

						 <Scene key="PurchaseVHRScreen"
	          component={RenewSubscriptionScreen}
						animation='ease-in'
						navigationBarStyle={styles.navbar} 
						hideNavBar={true}
						renderBackButton ={() => renderBackButton()}
						
					
				 />

				 	 <Scene key="Payment"
	          component={Paystack}
						animation='ease-in'
						navigationBarStyle={styles.navbar} 
						hideNavBar={true}
						renderBackButton ={() => renderBackButton()}
					
				 />

				 	 	 <Scene key="VerifyPay"
	          component={VerifyPay}
						animation='ease-in'
						navigationBarStyle={styles.navbar} 
						hideNavBar={true}
						renderBackButton ={() => renderBackButton()}
					
				 />

				  	 <Scene key="ForeignReportScreen"
	          component={ForeignReport}
						animation='ease-in'
						navigationBarStyle={styles.navbar} 
						hideNavBar={true}
						renderBackButton ={() => renderBackButton2()}
						
					
				 />


				 	 <Scene key="RiskAnalyzerScreen"
	          component={RiskAnalyzerScreen}
						animation='ease-in'
						navigationBarStyle={styles.navbar} 
						hideNavBar={true}
						
				/>

					 <Scene key="ProfileScreen"
	          component={ProfileScreen}
						animation='ease-in'
						navigationBarStyle={styles.navbar} 
						hideNavBar={true}
					
				/>

         <Scene key="AboutUs"
	          component={AboutUsScreen}
						animation='ease-in'
						navigationBarStyle={styles.navbar} 
						hideNavBar={true}
					
				/>

         <Scene key="ContactUs"
	          component={ContactScreen}
						animation='ease-in'
						navigationBarStyle={styles.navbar} 
						hideNavBar={true}
					
				/>

				  <Scene key="Faq"
	                    component={Faq}
						animation='ease-in'
						navigationBarStyle={styles.navbar} 
						hideNavBar={true}
					
				/>
                 

				   <Scene key="Notification"
	                     component={NotificationScreen}
						animation='ease-in'
						navigationBarStyle={styles.navbar} 
						hideNavBar={true}
					
				/>

					 <Scene key="Shazam"
	          component={ShazamScreen}
						animation='fade'
						navigationBarStyle={styles.navbar}
						hideNavBar={true}/>
					

				</Drawer>
				</Scene>
	    </Router> 
			</Root>
      </View>
			</Provider>
  );
  }
}




