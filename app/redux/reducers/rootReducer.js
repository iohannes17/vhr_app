
/**
 * @class rootReducer
 * It manages all reducers in the state
 */



import {combineReducers} from 'redux';
import loginReducer from './loginReducer';
import fetchVinHistoryReducer from './fetchVinReducer';
import fetchVinTeaserReducer from './TeaserReducer';
import UserInfoReducer from './userInfoReducer';
import registerReducer from './registerReducer';
import ActivePlanReducer from './activePlanReducer';
import PaymentReducer from './paymentReducer';
import ForeignReducer from './ForeignReducer';
import PaystackReducer from './paystackReducer';
import ShazamReducer from './ShazamReducer';
import NotificationReducer from './NotificationReducer';



const appReducer = combineReducers({
   userLog: loginReducer,
   vinhistory: fetchVinHistoryReducer,
   vinteaser: fetchVinTeaserReducer,
   userinfo: UserInfoReducer,
   userreg: registerReducer,
   activeplan: ActivePlanReducer,
   Confirmpayment: PaymentReducer,
   Foreignreport:ForeignReducer,
   response: PaystackReducer,
   shazamInfo:ShazamReducer,
   NotificationData: NotificationReducer
});


export default rootReducers = (state,action) =>{
    
    if(action.type === 'RESET'){

        state = undefined
    }

    return appReducer(state,action)
}