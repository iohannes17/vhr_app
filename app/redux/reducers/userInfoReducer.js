/**
 * This reducer updates state of users information
 * 
 */


import {FETCH_USERINFO_SUCCESS,FETCH_USERINFO_ERROR} from '../actions/types';



const initialState ={
   userInfoList:'',
   isAvailable: false,
   isFetching: true,
};


const UserInfoReducer = (state = initialState, action) =>{
   
 
    switch(action.type){


         case FETCH_USERINFO_ERROR:
              return{...state, userInfoList:action.payload,};

         case FETCH_USERINFO_SUCCESS:
         
              return {...state,userInfoList:action.payload,};


         default:
            return state;     
    }

};
 export default UserInfoReducer;